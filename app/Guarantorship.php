<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Guarantorship extends Model
{
    use SoftDeletes;

    protected $table = 'guarantorship';

    public $fillable = ['memberId', 'loanId', 'guarantorId', 'amount', 'status'];
}
