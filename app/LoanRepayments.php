<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LoanRepayments extends Model
{
    use SoftDeletes;

    protected $table = 'loanpayment';

    public $fillable = ['memberId', 'loanId', 'amount', 'added_by'];
}
