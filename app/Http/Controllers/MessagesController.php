<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Messages;

class MessagesController extends Controller
{

   public function __construct()
   {
       $this->middleware('auth');
   }
  public function messages()
  {
      $members = User::all();
      return view('messages', compact('members'));
  }
  public function send_messages(Request $request)
  {
      $mobile = $request->input('phone');
      $message = $request->input('single_message');
      $this->sms($mobile, $message);
      $save_sms = Messages::create([
          'added_by'  => \Auth::user()->id,
          'message'    => $message,
          'memberId'    => 'All'
        ]);

      return $save_sms;
  }
  public function send_messages_all(Request $request){
      $message  = $request->input('message');

      $numbers = User::all();
      die(var_dump($numbers));
      foreach ($numbers as $key) {
          $num = $key->phone;
          $this->sms($num, $message);
      }

      $save_sms = Messages::create([
          'added_by'  => \Auth::user()->id,
          'message'    => $message,
          'memberId'    => 'All'
        ]);
      return $save_sms; 
  }

  // public function send_messages(Request $request)
  // {
  //     $mobile = $request->input('phone');
  //     $message = $request->input('single_message');
  //     $this->sms($mobile, $message);
      
  // }
  public function sms($mobile, $message){
        $username = "neptune"; //username for your organisation
        $password = "neptune";
        $apiKey = "5d43fac077b64"; //apikey for your organisation
        $shortcode = 'NEPTUNE'; //assigned sender ID
        $method = 'sendsms'; // method to invoke{sendsms - to send SMS | balance - to check credit balance}

        $finalURL = "http://meppsms.meppcommunications.com/?username=" . urlencode($username) . "&password=" . urlencode($password) . "&apiKey=" . urlencode($apiKey) . "&message=" . urlencode($message) . "&senderID=".$shortcode."&msisdn=".$mobile."&method=".$method;

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $finalURL,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",

        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          echo $response;
        }
    }
}
