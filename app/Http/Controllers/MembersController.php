<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Savings;
use Mail;
use Auth;
use DB;
use PDF;
use Yajra\Datatables\DataTables;
class MembersController extends Controller
{
    public function all_members(){
        return view('all_members');
    }
    public function add_members(Request $request){
      //generate membershipNo
      $memNo          = User::orderBy('id', 'desc')->value('id');
      $memNos         = $memNo+1;
      $membershipNo   = "B000".$memNos;

      $member                 = $request->input('name');
      $amount                 = $request->input('savings');
      // $member              = User::where('id', $id)->value('name');
      $email                  = $request->input('email');
      // $email               = User::where('id', $id)->value('email');
      $message                = "Thank you for joining Neptune, your password is \"123abc\" please change it when you login. Click the link below to login. Your Membership Number is - ".$membershipNo.". Attached is your receipt of ".$amount;
      $smsMessage             = $member. ", Thank you for joining Neptune, your password is \"123abc\" please change it when you login. Your Membership Number is - ".$membershipNo.". Attached is your receipt ".$amount;

      $data['name']         =   $member;
      $data['to']           =   $email;
      $data['subject']      =   "New Member";
      $data['mailmessage']  =   $message;
      $data['sender']       =   "info@neptune.co.ke";
      $data['sendername']   =   "NEPTUNE Investements Group";
      $data['template']     =   "email.email";
      $data['link']         =   "http://nsig.co.ke/login";

      // $mobile = User::where('id', $id)->value('phone'); // recipient
      $mobile = $request->input('phone'); // recipient


      $idNumber = User::where('idNumber', $mobile)->count();
      $email    = User::where('email', $email)->count();

      if ($idNumber >= 1) {
        return 1;
      } elseif ($email >= 1) {
        return 2;
      } else {
        $mem = User::create([
          'membershipNo' => $membershipNo,
          'name'         => $request->input('name'),
          'email'        => $request->input('email'),
          'gender'       => $request->input('gender'),
          'idNumber'     => $request->input('idNumber'),
          'dob'          => $request->input('dob'),
          'phone'        => $request->input('phone'),
          'savings'       => $request->input('amount'),
          'password'     => bcrypt('123abc'),
          'memberType'   => 'member',
          'added_by'     => \Auth::user()->id
        ]);

        // Get Member ID
        $data['memberId']     =   User::orderBy('id', 'desc')->value('id');

        //generate receiptNo
        $receipt = Savings::orderBy('id', 'desc')->value('id');
        $receipts = $receipt+1;
        $data['receiptNo']    =   "NIG-".$receipts;
        $data['pdfName']      =   "Receipt-".time().'-'.$member.'.pdf';
        $a = $request->input('amount');
        $t = $a - 1500;
        $data['datePaid']     =   date('Y-m-d');

          $savings = Savings::create([
            'memberId'        => $data['memberId'],
            'savings'         => $t,
            'total_amount'    => $a,
            'datePaid'        => $data['datePaid'],
            'transactionRef'  => "Cash",
            'added_by'        => \Auth::user()->id,
            'receiptNo'       => $data['receiptNo'],
            'receiptPdf'      => $data['pdfName']
          ]);

        // $this->sms($mobile, $smsMessage);
        // // $this->ssendEmail($data);


        // Mail::send($data['template'],$data, function($message) use ($data){
        //   $member = DB::table('users')
        //                 ->join('savings', 'savings.memberId', '=', 'users.id')
        //                 ->where('users.id',$data['memberId'])
        //                 ->where('savings.datePaid',$data['datePaid'])
        //                 ->where('savings.receiptPdf',$data['pdfName'])
        //                 ->where('savings.receiptnO',$data['receiptNo'])
        //                 ->select('users.amount as initial', 'users.name', 'users.membershipNo', 'users.idNumber', 'users.email', 'savings.*')
        //                 ->orderBy('savings.id','desc')
        //                 ->get();
          // $pdf = PDF::loadView('pdf.initial', compact('member'))->save( 'D:\Projects\nsig\public\pdf\/'.$data['pdfName'].'.pdf' );
        //    $message->to($data['to'],$data['name'])->subject($data['subject']);
        //    $message->attachData($pdf->output(), 'D:\Projects\nsig\public\pdf\/'.$data['pdfName']);
        //    $message->from($data['sender'],$data['sendername']);
        //    // return 1;
        // });
      }

// return 1;
      // return $members;
        // return view('add_members');
    }
  public function sendEmail($data){
    Mail::send($data['template'],$data, function($message) use ($data){
       $message->to($data['to'],$data['name'])->subject
          ($data['subject']);
       $message->from($data['sender'],$data['sendername']);
    });
 }
    public function sms($mobile, $message){
      $username = "Neptune"; //username for your organisation
      $password = "neptune1";
      $apiKey = "5b445283565b0"; //apikey for your organisation
      $shortcode = "MEPP"; //assigned sender ID
      $method = 'sendsms'; // method to invoke{sendsms - to send SMS | balance - to check credit balance}

      $finalURL = "http://meppsms.meppcommunications.com/?username=" . urlencode($username) . "&password=" . urlencode($password) . "&apiKey=" . urlencode($apiKey) . "&message=" . urlencode($message) . "&senderID=".$shortcode."&msisdn=".$mobile."&method=".$method;

      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => $finalURL,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",

      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        echo $response;
      }
    }
    public function show_members(){
    $members = DB::table('users')->where('memberType', 'member')->get();
    return DataTables::of($members)
    ->addColumn('action', function ($value) {
      return '
      <button
      style=""
      data-toggle="modal"
      data-target="#addNewMemberModal"
      class="open-modal btn btn-sm btn-primary"
      value="'.$value->id.'"
      onclick="edit_member(' . "'" . $value->id . "'" . ')">
      Edit <i class="icon-options"></i>
      </button>
      <button
      style=""
      data-toggle="modal"
      data-target=".bs-example-modal-lg"
      class="open-modal btn btn-sm btn-danger"
      value="'.$value->id.'"
      onclick="cancel_request(' . "'" . $value->id . "'" . ')">
      Delete <i class="icon-close"></i>
      </button>
       ';
    })
    ->make(true);
    }
    public function get_member($member_id){
        return User::where('id', $member_id)->get();
    }
    public function edit_members(){
        return view('edit_members');
    }
    public function delete_members(){
        return view('delete_members');
    }
    public function active_members(){
        return view('active_members');
    }
    public function inactive_members(){
        return view('inactive_members');
    }
    public function member_savings(){
        return view('member_savings');
    }
    public function pending_savings(){
        return view('pending_savings');
    }

    public function approve_savings($savings_id){
      
      $memberId       =   Savings::where('id', $savings_id)->value('memberId');
      $membershipNo   =   User::where('id', $memberId)->value('membershipNo');

      $member               =   User::where('id', $memberId)->value('name');
      // die(var_dump($member));
      $amount               =   Savings::where('id', $savings_id)->value('savings');
      // $member            =   User::where('id', $id)->value('name');
      $email                =   User::where('id', $memberId)->value('email');
      // $email             =   User::where('id', $id)->value('email');
      $message              =   "Thank you for your contribution to Neptune Investment Group. Attached is your receipt of ".$amount;
      $smsMessage           =   $member. ", Thank you for your contribution to Neptune Investment Group. Attached is your receipt of ".$amount;

      $data['name']         =   $member;
      $data['to']           =   $email;
      $data['subject']      =   "Contribution";
      $data['membershipNo'] =   $membershipNo;
      $data['mailmessage']  =   $message;
      $data['sender']       =   "info@neptune.co.ke";
      $data['sendername']   =   "NEPTUNE Investements Group";
      $data['template']     =   "email.savings";
      $mobile = User::where('id', $memberId)->value('phone');
        // Get Member ID
        $data['memberId']     =   Savings::where('id', $savings_id)->value('memberId');

        $receipts = Savings::where('id', $savings_id)->value('id');
        $data['receiptNo']    =   "NIG-".$receipts;
        $receiptNo    =   "NIG-".$receipts;
        $data['pdfName']      =   time().'-'.$member.'.pdf';
        $pdfName      =   time().'-'.$member.'.pdf';
          
        
        $data['datePaid']     =   Savings::where('id', $savings_id)->value('datePaid');
        $datePaid     =   Savings::where('id', $savings_id)->value('datePaid');

      $pending = Savings::where('id', $savings_id)->update([ 'status' => 1, 'datePaid' => $datePaid, 'receiptPdf' => $pdfName, 'receiptNo' => $receiptNo ]);

        // $member = DB::table('users')
        //                 ->join('savings', 'savings.memberId', '=', 'users.id')
        //                 ->where('users.id',$memberId)
        //                 ->where('savings.datePaid',$datePaid)
        //                 ->where('savings.receiptPdf',$pdfName)
        //                 ->where('savings.receiptNo',$receiptNo)
        //                 ->select('users.amount as initial', 'users.name', 'users.membershipNo', 'users.idNumber', 'users.email', 'savings.*')
        //                 ->orderBy('savings.id','desc')
        //                 ->get();

        // var_dump($member);
        // die();
        // $this->sms($mobile, $smsMessage);

        // Mail::send($data['template'],$data, function($message) use ($data){
        //   $member = DB::table('users')
        //                 ->join('savings', 'savings.memberId', '=', 'users.id')
        //                 ->where('users.id',$data['memberId'])
        //                 ->where('savings.datePaid',$data['datePaid'])
        //                 ->where('savings.receiptPdf',$data['pdfName'])
        //                 ->where('savings.receiptNo',$data['receiptNo'])
        //                 ->select('users.amount as initial', 'users.name', 'users.membershipNo', 'users.idNumber', 'users.email', 'savings.*')
        //                 ->orderBy('savings.id','desc')
        //                 ->get();
        //   $pdf = PDF::loadView('pdf.savings', compact('member'))->save( 'D:\Projects\nsig\public\pdf\/'.$data['pdfName'].'.pdf' );
        //    $message->to($data['to'],$data['name'])->subject($data['subject']);
        //    $message->attachData($pdf->output(), 'D:\Projects\nsig\public\pdf\/'.$data['pdfName'].'.pdf');
        //    $message->from($data['sender'],$data['sendername']);
           
        // });
      
      return $pending;
    }

    public function savings(){
        $members = User::where('memberType', 'member')->orderBy('id', 'desc')->get();
        return view('all_savings', compact('members'));
    }
    public function show_savings(){
    $savings = DB::table('savings')
                    ->join('users', 'users.id', '=', 'savings.memberId')
                    ->where('savings.status', 1)
                    ->select('users.name', 'users.idNumber', 'savings.*')
                    ->orderBy('savings.id','desc')
                    ->get();
    return DataTables::of($savings)
    ->addColumn('action', function ($value) {
      return '
      <button
      style=""
      data-toggle="modal"
      data-target="#addNewSavingsModal"
      class="open-modal btn btn-sm btn-primary"
      value="'.$value->id.'"
      onclick="edit_member(' . "'" . $value->id . "'" . ')">
      Edit <i class="icon-options"></i>
      </button>
      <button
      style=""
      data-toggle="modal"
      data-target=".bs-example-modal-lg"
      class="open-modal btn btn-sm btn-danger"
      value="'.$value->id.'"
      onclick="cancel_request(' . "'" . $value->id . "'" . ')">
      Delete <i class="icon-close"></i>
      </button>
       ';
    })
    ->make(true);
    }

    public function show_pending_savings(){
    $savings = DB::table('savings')
                    ->join('users', 'users.id', '=', 'savings.memberId')
                    ->where('savings.status', 0)
                    ->select('users.name', 'users.idNumber', 'savings.*')
                    ->orderBy('savings.id','desc')
                    ->get();
    return DataTables::of($savings)
    ->addColumn('action', function ($value) {
      return '
      <button
      class="btn btn-sm btn-primary"
      value="'.$value->id.'"
      onclick="approve(' . "'" . $value->id . "'" . ')">
      Approve <i class="icon-options"></i>
      </button>
      <button
      style=""
      data-toggle="modal"
      data-target=".bs-example-modal-lg"
      class="open-modal btn btn-sm btn-danger"
      value="'.$value->id.'"
      onclick="cancel_request(' . "'" . $value->id . "'" . ')">
      Delete <i class="icon-close"></i>
      </button>
       ';
    })
    ->make(true);
    }

        public function downloadPDF(){
          $receipt = Savings::orderBy('id', 'desc')->value('id'); // recipient
$receipts = $receipt+1;
          $data['receiptNo']    =   "nig-".$receipts;
          // echo $data['receiptNo'];
          echo $data['receiptNo'];
          // $user = UserDetail::find($id);

          // $pdf = PDF::loadView('pdf.savings')->save( 'D:\Projects\nsig\public\pdf\receipt.pdf' );
          // return $pdf->download('receipt.pdf');

        }

    public function add_savings(Request $request){
      // $mobile = User::where('id', $request->input('memberId'))->value('phone');
      // printf($mobile);
      // die();

      $member                 = User::where('id', $request->input('memberId'))->value('name');
      $membershipNo           = User::where('id', $request->input('memberId'))->value('membershipNo');
      // $member              = User::where('id', $id)->value('name');
      $email                  = User::where('id', $request->input('memberId'))->value('email');
      // $email               = User::where('id', $id)->value('email');
      $message                = "Your contribution of ". $request->input('amount'). ", to Neptune Investments Group has been well received.";
      $smsMessage             = $member. ", Your contribution of ". $request->input('amount'). ", to Neptune Investments Group has been well received.";

      $data['name']         =   $member;
      $data['to']           =   $email;
      $data['subject']      =   "Savings";
      $data['mailmessage']  =   $message;
      $data['membershipNo'] =   $membershipNo;
      $data['sender']       =   "info@neptune.co.ke";
      $data['sendername']   =   "NEPTUNE Investments Group";
      $data['template']     =   "email.savings";
      $data['datePaid']     =   $request->input('datePaid');
      $data['memberId']     =   $request->input('memberId');
      $data['pdfName']      =   time().'-'.$member.'.pdf';

      // $mobile = User::where('id', $id)->value('phone'); // recipient
      $mobile = User::where('id', $request->input('memberId'))->value('phone'); // recipient

      //generate receiptNo
      $receipt = Savings::orderBy('id', 'desc')->value('id');
      $receipts = $receipt+1;
      $data['receiptNo']    =   "NIG-".$receipts;

        $savings = Savings::create([
          'memberId'        => $request->input('memberId'),
          'total_amount'    => $request->input('amount'),
          'savings'         => $request->input('amount'),
          'datePaid'        => $request->input('datePaid'),
          'transactionRef'  => $request->input('transactionRef'),
          'status'          => 1,
          'added_by'        => \Auth::user()->id,
          'receiptNo'       => $data['receiptNo'],
          'receiptPdf'      => $data['pdfName']
        ]);

        // $this->sms($mobile, $smsMessage);

            // Mail::send($data['template'],$data, function($message) use ($data){
            //   $member = DB::table('users')
            //                 ->join('savings', 'savings.memberId', '=', 'users.id')
            //                 ->where('users.id',$data['memberId'])
            //                 ->where('savings.datePaid',$data['datePaid'])
            //                 ->where('savings.receiptPdf',$data['pdfName'])
            //                 ->where('savings.receiptnO',$data['receiptNo'])
            //                 ->orderBy('savings.id','desc')
            //                 ->get();
            //   $pdf = PDF::loadView('pdf.savings', compact('member'))->save( 'D:\Projects\nsig\public\pdf\/'.$data['pdfName'].'.pdf' );
            //    $message->to($data['to'],$data['name'])->subject($data['subject']);
            //    $message->attachData($pdf->output(), 'D:\Projects\nsig\public\pdf\/'.$data['pdfName'].'.pdf');
            //    $message->from($data['sender'],$data['sendername']);
            // });
        return $savings;
    }
}
