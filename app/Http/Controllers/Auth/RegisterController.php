<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|max:255',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
      $member                 = $data['name'];
      $email                  = $data['email'];
      $message                = "You have been added as an admin of NSIG, your email is ". $email." and password is \"123abc\" please change it when you login. Click the link below to login.";
      $smsMessage             = $member. ", You have been added as an admin of NSIG, your email is ". $email." and password is \"123abc\" please change it when you login.";

      $email_data['name']=$member;
      $email_data['to']=$email;
      $email_data['subject']="New Admin";
      $email_data['mailmessage']=$message;
      $email_data['sender']="info@nsig.co.ke";
      $email_data['sendername']="NEPTUNE";
      $email_data['template']="email.email";
      $email_data['link']="http://nsig.co.ke/login";

      $mobile = $data['phone']; // recipient

        $this->sms($mobile, $smsMessage);
        $this->sendEmail($email_data);

        return User::create([
            'name'  => $data['name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'memberType' => $data['memberType'],
            'password' => bcrypt($data['password']),
        ]);

    }

  public function sendEmail($email_data){
    Mail::send($email_data['template'],$email_data, function($message) use ($email_data){
       $message->to($email_data['to'],$email_data['name'])->subject
          ($email_data['subject']);
       $message->from($email_data['sender'],$email_data['sendername']);
    });
 }
    public function sms($mobile, $message){
      $username = "Neptune"; //username for your organisation
      $password = "neptune1";
      $apiKey = "5b445283565b0"; //apikey for your organisation
      $shortcode = "MEPP"; //assigned sender ID
      $method = 'sendsms'; // method to invoke{sendsms - to send SMS | balance - to check credit balance}

      $finalURL = "http://meppsms.meppcommunications.com/?username=" . urlencode($username) . "&password=" . urlencode($password) . "&apiKey=" . urlencode($apiKey) . "&message=" . urlencode($message) . "&senderID=".$shortcode."&msisdn=".$mobile."&method=".$method;

      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => $finalURL,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",

      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        echo $response;
      }
    }
}
