<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function member_profile(){
      return view('profile');
    }

    public function updatePassport(Request $request){
      $profile_data = $request->all();
      $id = $profile_data['member_id'];
        $files =[];
        if ($request->file('passport')) $files[] = $request->file('passport');
        if ($request->file('nationalID')) $files[] = $request->file('nationalID');
        foreach ($files as $file)
        {
          if($file == 'passport'){
              $filename = time().'-'.$id.'-'.$file->getClientOriginalName();
              $file->move('ids', $filename.'-'.$file->getClientOriginalName());
                  User::where('id', $id)->update([ 'passport' => $filename]);
            }else {
              $filename = time().'-'.$id.'-'.$file->getClientOriginalName();
              $file->move('ids', $filename.'-'.$file->getClientOriginalName());
                  User::where('id', $id)->update([ 'natID' => $filename]);
            }
        }

        // if ($request->file('nationalID')) $files[] = $request->file('nationalID');
        // foreach ($files as $file)
        // {
        //   if(!empty($file)){
        //       $filename = time().'-'.$id.'-'.$file->getClientOriginalName();
        //       $file->move('ids', $filename.'-'.$file->getClientOriginalName());
        //           User::where('id', $id)->update([ 'nationalID' => $filename]);
        //     }
        // }

            return redirect()->back();
        }

    public function createProfile(Request $request){
      // print_r ($request->input('userId'));
      $profile_data = $request->all();
      $fullName = $profile_data['name'];

      if ($request->file('avatar') === null ) {
        $name = \Auth::user()->avatar;
      } else {
        $file = $request->file('avatar');
        $file->move('avatar', time().'-'.$fullName.'-'.$file->getClientOriginalName());
        $name = time().'-'.$fullName.'-'.$file->getClientOriginalName();
      }

      $id = $profile_data['member_id'];

      if ($profile_data['password'] === null) {
          User::where('id', $id)->update([
            'name'        =>  $profile_data['name'],
            'email'       =>  $profile_data['email'],
            'phone'       =>  $profile_data['phone'],
            'idNumber'    =>  $profile_data['idNumber'],
            'dob'         =>  $profile_data['dob'],
            'avatar'      =>  $name]);
      } else {
          User::where('id', $id)->update([
              'name'        =>  $profile_data['name'],
              'password'    =>  bcrypt($profile_data['password']),
              'email'       =>  $profile_data['email'],
              'phone'       =>  $profile_data['phone'],
              'idNumber'    =>  $profile_data['idNumber'],
              'dob'         =>  $profile_data['dob'],
              'avatar'      =>  $name]);
      }

      return redirect()->back();
    }
}
