<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Yajra\Datatables\DataTables;
use App\LoanTypes;
use App\Loans;
use App\Guarantorship;
use App\User;

class LoansController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('loans.all_loans');
    }

    public function paid_loans()
    {
        return view('loans.all_loans');
    }

    public function loan_types(){
        return view('loans.loan_types');
    }

    public function apply_loan(){
        $guarantors = User::where('memberType', 'member')->get();
        $loan_types = LoanTypes::all();
        return view('loans.apply_loan', compact('guarantors','loan_types'));
    }
    
    public function add_loan_application(Request $request){

        $check1 = Loans::where('memberId', Auth::user()->id)
                        ->where('status', 'Pending')
                        ->count();

        $check2 = Loans::where('memberId', Auth::user()->id)
                        ->Where('status', 'Approved')
                        ->count();

        if (($check1 >= 1) || ($check2 >= 1)) {
            return 1;
        }
        
        $guarantors = serialize($request->guarantors);

        $guarantor = $request->guarantors;
        
        $loan_types = Loans::create([
            'memberId'              => Auth::user()->id,
            'loan_type_id'          => $request->input('loan_type'),
            'amount'                => $request->input('loan_amount'),
            'balance'               => $request->input('loan_amount'),
            'startDate'             => $request->input('startDate'),
            'expectedFinishDate'    => $request->input('expectedFinishDate'),
            'guarantors'            => $guarantors,
            'status'                => 'Pending'
          ]);
        $loanId = Loans::where('memberId', Auth::user()->id)
                            ->where('guarantors', $guarantors)
                            ->orderBy('id', 'desc')
                            ->value('id');

        foreach($guarantor as $guarantorId) {
            Guarantorship::create([
                'loanId'        => $loanId,
                'guarantorId'   => $guarantorId,
                'memberId'      => Auth::user()->id,
                'status'        => 'Pending'
            ]);
            $mobile = User::where('id', $guarantor)->value('phone');
            $member = User::where('id', $guarantor)->value('name');
            $smsMessage             = $member. ", You have been requested by ". Auth::user()->name. ", to guarantee a loan, Kindly login to approve. Thank you.";
            $this->sms($mobile, $smsMessage);
        }

        return $loan_types;
    }

    public function add_loan_type(Request $request){
      
    $loan_types = LoanTypes::create([
        'name'              => $request->input('name'),
        'interest'          => $request->input('interest'),
        'processing_fee'    => $request->input('processing_fee'),
        'description'       => $request->input('description')
      ]);
  
      return $loan_types;
    }

    public function show_loan_types(){
        $loan_types = DB::table('loan_types')->get();
        return DataTables::of($loan_types)
        ->addColumn('action', function ($value) {
        return '<button
        class="btn btn-sm btn-primary"
        value="'.$value->id.'"
        onclick="approve(' . "'" . $value->id . "'" . ')">
        Edit <i class="icon-options"></i>
        </button>
        <button
        style=""
        data-toggle="modal"
        data-target=".bs-example-modal-lg"
        class="open-modal btn btn-sm btn-danger"
        value="'.$value->id.'"
        onclick="cancel_request(' . "'" . $value->id . "'" . ')">
        Delete <i class="icon-close"></i>
        </button>';
        })
        ->make(true);
    }

    public function all_member_loans(){
        return view('loans.all_member_loans');
    }
    public function show_member_loan_guarantors($loan_id){
        $loan = Loans::where('id', $loan_id)->value('guarantors');
        $approved = Loans::where('id', $loan_id)->value('status');
        $guarantors = unserialize($loan);
        $loanId = $loan_id;

        return var_dump($loan);
        die();
        return view('loans.show_member_loan', compact('guarantors', 'loanId', 'approved'));
    }

    public function show_member_loans(){
        if (Auth::user()->memberType == 'admin' || Auth::user()->memberType == 'treasurer') {
            $loans = DB::table('loans')->get();
        } else {
            $loans = DB::table('loans')->where('memberId', Auth::user()->id)->get();
        }
        
        return DataTables::of($loans)
        ->addColumn('action', function ($value) {
        return '<button
        class="btn btn-sm btn-primary"
        value="'.$value->id.'"
        onclick="approve(' . "'" . $value->id . "'" . ')">
        Edit <i class="icon-options"></i>
        </button>
        <button
        style=""
        data-toggle="modal"
        data-target=".bs-example-modal-lg"
        class="open-modal btn btn-sm btn-danger"
        value="'.$value->id.'"
        onclick="cancel_request(' . "'" . $value->id . "'" . ')">
        Delete <i class="icon-close"></i>
        </button>
        <a
        style=""
        href="loan/'.$value->id.'"
        class="open-modal btn btn-sm btn-success"
        value="'.$value->id.'">
        View <i class="icon-close"></i>
        </a>';
        })
        ->make(true);
    }

    public function guarantor_requests(){
        // $loan_requests = Guarantorship::where('status','Pending')->where('guarantorId', Auth::user()->id);
        $loan_requests = DB::table('guarantorship')
                            ->join('users', 'users.id', '=', 'guarantorship.guarantorId')
                            ->join('loans', 'loans.id', '=', 'guarantorship.loanId')
                            ->where('guarantorship.status','Pending')->where('guarantorship.guarantorId', Auth::user()->id)
                            ->select('guarantorship.id as id', 'users.name as name', 'loans.amount as amount', 'users.phone as phone', 'guarantorship.loanId as loan_id')
                            ->get();
                            // var_dump($loan_requests);
                            // die();
        return view('loans.guarantor_requests', compact('loan_requests'));
    }

    public function guarantor_approval(Request $request){
        // check if member has loans or has guaranteed anyone or if he/she has enough to guarantee
        $check = Loans::where('id', $request->input('approve_loan_id'))
                        ->where('status', 'Pending')
                        ->where('memberId', Auth::user()->id)
                        ->count();
        $savings = Savings::where('memberId', Auth::user()->id)->sum('savings');
        $guarantor_sum = DB::table('guarantorship')
                            ->join('loans', 'loans.id', '=', 'guarantorship.loansId')
                            ->where('guarantorship.memberId', Auth::user()->id)
                            ->where('loans.balance', '!=', 0)
                            ->where('loans.status', 'Approved')
                            ->where('guarantorship.status', 'Approved')
                            ->sum('amount');
        $available_funds = 0.5 * ($savings - $guarantor_sum);

        if ($check >= 1) {
            // $check1 = Loans::where('memberId', Auth::user()->id)
            //             ->where('status', 'Pending')
            //             ->value('balance');
            return 2;
        } else {
            $check2 = DB::table('guarantorship')
                            ->join('loans', 'loans.id', '=', 'guarantorship.loansId')
                            ->where('guarantorship.memberId', Auth::user()->id)
                            ->where('loans.status', 'Approved')
                            ->count();
            if ($check2 == 3) {
                return 3;
            } else {
                if ($available_funds >= $request->input()) {
                return Guarantorship::where('loanId', $request->input('approve_loan_id'))
                                ->where('memberId', Auth::user()->id)
                                ->update([ 'status' => 'Approved', 'amount' => $request->input('amount')]);
                } else {
                    return 4;
                }
                
            }
            
        }
        
        
    }

    public function guarantor_denial(Request $request){
        
    }

    public function approve_loan($loan_id){
        $memberId   = Loans::where('id', $loan_id)->value('memberId');
        $mobile     = User::where('id', $memberId)->value('phone'); 
        $name       = User::where('id', $memberId)->value('name'); 

        $guarantors       = Loans::where('id', $loan_id)->value('guarantors');
        $guarantors_array = unserialize($guarantors);

        foreach ($guarantors_array as $value) {
            $check = Guarantorship::where('loanId', $loan_id)->where('guarantorId', $value)->value('status');

            if ($check == 'Pending' || $check == 'Denied') {
                return 1;
            }
        }

        $approve    = Loans::where('id', $loan_id)->update([ 'status' => 'Approved', 'comment' => 'Loan Approved']);
        $smsMessage = $name. ", Your loan request has been approved, It will reflect in your account in the next 3 business days. Thank you.";
        $this->sms($mobile, $smsMessage);
        return $approve;
    }
    
    public function deny_loan(Request $request, $loan_id){
        $memberId   = Loans::where('id', $loan_id)->value('memberId');
        $mobile     = User::where('id', $memberId)->value('phone'); 
        $name       = User::where('id', $memberId)->value('name'); 
        $deny       = Loans::where('id', $loan_id)->update([ 'status' => 'Denied', 'comment' => $request->input('comment')]);
        $smsMessage = $name. ", Your loan request has been denied because ".$request->comment. ", Kindly look into it then applly again. Thank you.";
        $this->sms($mobile, $smsMessage);
        return $deny;
    }

    public function sms($mobile, $message){
        $username = "Neptune"; //username for your organisation
        $password = "neptune1";
        $apiKey = "5b445283565b0"; //apikey for your organisation
        $shortcode = "MEPP"; //assigned sender ID
        $method = 'sendsms'; // method to invoke{sendsms - to send SMS | balance - to check credit balance}
  
        $finalURL = "http://meppsms.meppcommunications.com/?username=" . urlencode($username) . "&password=" . urlencode($password) . "&apiKey=" . urlencode($apiKey) . "&message=" . urlencode($message) . "&senderID=".$shortcode."&msisdn=".$mobile."&method=".$method;
  
        $curl = curl_init();
  
        curl_setopt_array($curl, array(
          CURLOPT_URL => $finalURL,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
  
        ));
  
        $response = curl_exec($curl);
        $err = curl_error($curl);
  
        curl_close($curl);
  
        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          echo $response;
        }
      }
}
