<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Savings;
use Yajra\Datatables\DataTables;

class MemberController extends Controller
{
   /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      return view('member.member');
  }

  public function add_savings(Request $request){

    $savings = Savings::create([
      'memberId'        => $request->input('memberId'),
      'savings'         => $request->input('savings'),
      'shares'          => $request->input('shares'),
      'loan'            => $request->input('loan'),
      'interest'        => $request->input('interest'),
      'fine'            => $request->input('fine'),
      'total_amount'    => $request->input('total_amount'),
      'datePaid'        => $request->input('datePaid'),
      'transactionRef'  => $request->input('transactionRef'),
      'status'          => 0
    ]);

    return $savings;
  }

  public function calculate(Request $request){
    $savings    = $request->savings;
    $loan       = $request->loan;
    $fine       = $request->fine;
    $interest   = $request->interest;
    $shares     = $request->shares;

    $total = $savings + $loan + $fine + $interest + $shares;

    return $total;
  }

  public function show_member_savings($id){
    $mSavings = DB::table('savings')
                    ->join('users','users.id', '=', 'savings.memberId')
                    ->where('savings.memberId', $id)
                    ->where('savings.status',1)
                    ->select('users.name', 'users.idNumber', 'savings.*')
                    ->get();
    return DataTables::of($mSavings)
      ->addColumn('action', function ($value) {
      return '<button
      style=""
      data-toggle="modal"
      data-target=".bs-example-modal-lg"
      class="open-modal btn btn-sm btn-danger"
      value="'.$value->receiptPdf.'"
      onclick="cancel_request(' . "'" . $value->receiptPdf . "'" . ')">
      Delete <i class="icon-close"></i>
      </button>
      <a href="D:\Projects\nsig\public\pdf/' . $value->receiptPdf . '"  download> Reciept in PDF </i> </a>
      ';
      })
      ->make(true);
  }
}
