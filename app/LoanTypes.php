<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LoanTypes extends Model
{
    use SoftDeletes;

    protected $table = 'loan_types';

    public $fillable = ['name', 'interest', 'processing_fee', 'description'];
}
