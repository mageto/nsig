<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Savings extends Model
{
    use SoftDeletes;

    protected $table = 'savings';

    public $fillable = ['memberId', 'savings', 'shares', 'loan', 'interest', 'fine', 'total_amount', 'datePaid', 'transactionRef', 'receiptPdf', 'receiptNo'];
}
