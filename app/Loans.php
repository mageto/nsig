<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Loans extends Model
{
    use SoftDeletes;

    protected $table = 'loans';

    public $fillable = ['memberId', 'amount', 'interest', 'total_amount', 'guarantors', 'balance', 'period', 'startDate', 'expectedFinishDate', 'finishDate', 'committedMonthlyInstalments', 'status', 'added_by'];
}
