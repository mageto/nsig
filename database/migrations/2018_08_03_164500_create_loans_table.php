<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('memberId');
            $table->string('amount');
            $table->string('guarantors');
            $table->string('balance');
            $table->string('period');
            $table->string('startDate');
            $table->string('expectedFinishDate');
            $table->string('finishDate');
            $table->string('committedMonthlyInstalments')->nullable();
            $table->string('status');
            $table->string('added_by')->nullable();
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
