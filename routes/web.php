<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing.index');
});
Route::get('/login', function () {
    return view('auth.login');
});

Route::get('/logout', 'Auth\LoginController@logout');
Route::get('email', 'EmailController@sendEmail');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function()
{
    // members routes
    Route::get('/all-members', 'MembersController@all_members');
    Route::post('/add-members', 'MembersController@add_members');
    Route::get('/get-member/{member_id}', 'MembersController@get_member');
    Route::get('/show-members', 'MembersController@show_members');
    Route::get('/edit-members', 'MembersController@edit_members');
    Route::get('/delete-members', 'MembersController@delete_members');
    Route::get('/active-members', 'MembersController@active_members');
    Route::get('/inactive-members', 'MembersController@inactive_members');

    // savings routes
    Route::get('/savings', 'MembersController@savings');
    Route::get('/pending-savings', 'MembersController@pending_savings');
    Route::get('/show-pending-savings', 'MembersController@show_pending_savings');
    Route::get('/approve-savings/{savings_id}', 'MembersController@approve_savings');
    Route::post('/add-savings', 'MembersController@add_savings');
    Route::get('/get-savings', 'MembersController@get_savings');
    Route::get('/show-savings', 'MembersController@show_savings');

    // loans routes
    Route::get('/all-loans', 'LoansController@all_member_loans');
    Route::get('/apply-loan', 'LoansController@apply_loan');
    Route::post('/add-loan-application', 'LoansController@add_loan_application');
    Route::get('/all-member-loans', 'LoansController@all_member_loans');
    Route::get('/show-member-loans', 'LoansController@show_member_loans');
    Route::get('/loan/{loan_id}', 'LoansController@show_member_loan_guarantors');
    Route::get('/paid-loans', 'LoansController@paid_loans');
    Route::get('/unpaid-loans', 'LoansController@unpaid_loans');
    Route::get('/pending-loan-requests', 'LoansController@life_members');
    Route::get('/loan-types', 'LoansController@loan_types');
    Route::get('/show-loan-types', 'LoansController@show_loan_types');
    Route::post('/add-loan-type', 'LoansController@add_loan_type');
    Route::post('/approve-loan/{loan_id}', 'LoansController@approve_loan');
    Route::post('/deny-loan/{loan_id}', 'LoansController@deny_loan');
    Route::get('/guarantor-requests', 'LoansController@guarantor_requests');
    Route::post('/guarantor-denial', 'LoansController@guarantor_denial');
    Route::post('/guarantor-approval', 'LoansController@guarantor_approval');

    //Messages routes
    Route::get('/messages', 'MessagesController@messages');
    Route::post('/send-message', 'MessagesController@send_messages');
    Route::post('/send-message-all', 'MessagesController@send_messages_all');

    // calculate
    Route::get('/calculate', 'MemberController@calculate');
});

// Members

Route::get('/member', 'MemberController@index')->name('member');
Route::get('/member-profile', 'HomeController@member_profile')->name('member-profile');
Route::post('/profile/create', 'HomeController@createProfile');
Route::post('/profile/passport', 'HomeController@updatePassport');

Route::group(['middleware' => 'auth'], function()
{
    Route::get('/show-member-savings/{id}', 'MemberController@show_member_savings');
    Route::get('/member-savings', 'MembersController@member_savings');
    Route::post('/member-add-savings', 'MemberController@add_savings');
});

Route::get('/downloadPDF','MembersController@downloadPDF');
