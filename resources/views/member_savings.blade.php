@extends('layouts.app')

@section('content')

<div class="row mb-5">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                Savings Management
            <button style="float:right !important;" type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addNewSavingsModal">
                Add New Savings
            </button>
          </div>
          <br>
            <div class="table-responsive">
              <table id="datatable-1" class="table table-datatable table-striped table-hover dataTable no-footer" role="grid" aria-describedby="datatable-1_info">
                  <thead class="thead-light">
                      <tr>
                        <th>#</th>
                          <th>Member</th>
                          <th>ID Number</th>
                          <th>Amount</th>
                          <th>Date</th>
                          <th class="text-right">Action</th>
                      </tr>
                  </thead>
                  <tbody></tbody>
								</table>

                <script type="text/javascript">
                  var table;
                  $(document).ready(function() {
                  var memberId        =  document.getElementById('memberId').value;

                  table = $('#datatable-1').DataTable({
                      processing: true,
                      serverSide: true,
                      responsive: true,
                      ajax: '{{ url("/show-member-savings") }}/'+ memberId,
                      columns: [
                      { data: 'id',        name: 'id' ,         searchable: false},
                      { data: 'name',      name: 'name' ,       searchable: true},
                      { data: 'idNumber',  name: 'idNumber' ,   searchable: true},
                      { data: 'savings',   name: 'savings' ,     searchable: true},
                      { data: 'datePaid',  name: 'datePaid' ,   searchable: true},
                      { data: "action" }
                      ],
                  });
                  });
              </script>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addNewSavingsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Savings</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <input class="form-control form-control-sm" id="memberId" name="memberId" type="hidden" value="{{ Auth::user()->id }}">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="savings">Savings</label>
                                    <input class="form-control form-control-sm" id="savings" name="savings" type="text" placeholder="0.00">
                                </div>
                                <div class="form-group">
                                    <label for="shares">Shares</label>
                                    <input class="form-control form-control-sm" id="shares" name="shares" type="text" placeholder="0.00">
                                </div>
                                <div class="form-group">
                                    <label for="loan">Loan Repayment</label>
                                    <input class="form-control form-control-sm" id="loan" name="loan" type="text" placeholder="0.00">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="interest">Interest</label>
                                    <input class="form-control form-control-sm" id="interest" name="interest" type="text" placeholder="0.00" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="fines">Fines</label>
                                    <input class="form-control form-control-sm" id="fine" name="fine" type="text" placeholder="0.00" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="total_amount">Total</label>
                                    <input class="form-control form-control-sm" id="total_amount" name="total_amount" type="text" placeholder="0.00" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="transactionRef">Transaction Ref:</label>
                            <input class="form-control form-control-sm" id="transactionRef" name="transactionRef" type="text" placeholder="Transaction Ref">
                        </div>
                        <div class="form-group">
                            <label for="datePaid">Date</label>
                            <input class="form-control form-control-sm" id="datePaid" name="datePaid" type="date">
                        </div>
                        <div class="form-group">
                            <label for="datePaid">Month</label>
                            <select class="form-control form-control-sm" id="memberId" name="memberId">
                            <option>Month</option>
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                    <button type="button" onclick="add_saving()" id="btn-save" class="btn btn-primary btn-sm">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$("input").keyup(function () {
    var savings    = document.getElementById('savings').value;
    var interest   = document.getElementById('interest').value;
    var shares     = document.getElementById('shares').value;
    var fine       = document.getElementById('fine').value;
    var loan       = document.getElementById('loan').value;
// alert('m');
    $.ajax({
          headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
        url : "/calculate",
        type: "get",
        data: {"savings" : savings, "interest" : interest, "shares" : shares, "fine" : fine, "loan" : loan},
        // dataType: "json",
        success: function(data)
        {
            document.getElementById('total_amount').value = data; 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
        console.log(errorThrown);
        }
    });
})
function edit_member(member_id){

$.get('/get-savings/' + member_id, function (data) {

    $('#btn-save').val("Update");
    console.log(data);

    $('#memberId').val(data[0].id).change();
    $('#amount').val(data[0].amount);
    $('#transactionRef').val(data[0].transactionRef);
    $('#datePaid').val(data[0].datePaid);
    });
}


function add_saving(){
    var memberId        =  document.getElementById('memberId').value;
    var savings         =  document.getElementById('savings').value;
    var shares          =  document.getElementById('shares').value;
    var loan            =  document.getElementById('loan').value;
    var interest        =  document.getElementById('interest').value;
    var fine            =  document.getElementById('fine').value;
    var total_amount    =  document.getElementById('total_amount').value;
    var transactionRef  =  document.getElementById('transactionRef').value;
    var datePaid        =  document.getElementById('datePaid').value;

    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
        }
    });

    $.ajax({
        url: '/member-add-savings', // point to server-side PHP script
        data: { memberId : memberId, savings : savings, shares : shares, loan : loan, interest : interest, fine : fine, total_amount : total_amount, transactionRef : transactionRef, datePaid : datePaid  } ,
        type: 'POST',
        success: function(data) {

                document.getElementById('memberId').val = "";
                document.getElementById('amount').val = "";
                document.getElementById('transactionRef').val = "";
                document.getElementById('datePaid').val = "";

                table.ajax.reload(null,false); //reload datatable ajax
                $('#addNewSavingsModal').modal('hide');
                toastr.success('Savings added successfully wait for approval.');

        },
        error: function (error, data) {
            console.log('Error:', data);
        }
    });
}
</script>
@endsection
