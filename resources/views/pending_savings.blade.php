@extends('layouts.app')

@section('content')

<div class="row mb-5">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                Pending Savings Management
            </div>
            <br>
            <div class="table-responsive">
              <table id="datatable-1" class="table table-datatable table-striped table-hover dataTable no-footer" role="grid" aria-describedby="datatable-1_info">
                  <thead class="thead-light">
                      <tr>
                        <th>#</th>
                          <th>Member</th>
                          <th>ID Number</th>
                          <th>Amount</th>
                          <th>Date</th>
                          <th class="text-right">Action</th>
                      </tr>
                  </thead>
                  <tbody></tbody>
								</table>

                <script type="text/javascript">
                  var table;

                  $(document).ready(function() {
                  table = $('#datatable-1').DataTable({
                      processing: true,
                      serverSide: true,
                      responsive: true,
                      ajax: '{{ url("/show-pending-savings") }}',
                      columns: [
                      { data: 'id',        name: 'id' ,         searchable: false},
                      { data: 'name',      name: 'name' ,       searchable: true},
                      { data: 'idNumber',  name: 'idNumber' ,   searchable: true},
                      { data: 'savings',   name: 'savings' ,    searchable: true},
                      { data: 'datePaid',  name: 'datePaid' ,   searchable: true},
                      { data: "action" }
                      ],
                  });
                  });
              </script>
            </div>
        </div>
    </div>
</div>

<script>
function approve(savings_id){

  $.ajax({
      url: '/approve-savings/' + savings_id, // point to server-side PHP script
      // data: { memberId : memberId  } ,
      type: 'GET',
      success: function(data) {

            toastr.success('Saving Approved Successfully');
            table.ajax.reload(null,false); //reload datatable ajax

      },
      error: function (error, data) {
          console.log('Error:', data);
      }
  });
}

function edit_member(member_id){

        $.get('/get-savings/' + member_id, function (data) {

            $('#btn-save').val("Update");
            console.log(data);

            $('#memberId').val(data[0].id).change();
            $('#amount').val(data[0].amount);
            $('#transactionRef').val(data[0].transactionRef);
            $('#datePaid').val(data[0].datePaid);
          });
        }
    function add_saving(){
      var memberId        =  document.getElementById('memberId').value;
      var amount          =  document.getElementById('amount').value;
      var transactionRef  =  document.getElementById('transactionRef').value;
      var datePaid        =  document.getElementById('datePaid').value;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
                }
            });

            $.ajax({
                url: '/add-savings', // point to server-side PHP script
                data: { memberId : memberId, amount : amount, transactionRef : transactionRef, datePaid : datePaid  } ,
                type: 'POST',
                success: function(data) {

                        document.getElementById('memberId').val = "";
                        document.getElementById('amount').val = "";
                        document.getElementById('transactionRef').val = "";
                        document.getElementById('datePaid').val = "";

                        table.ajax.reload(null,false); //reload datatable ajax
                        $('#addNewSavingsModal').modal('hide');
                        toastr.success('Savings added successfully');

                },
                error: function (error, data) {
                    console.log('Error:', data);
                }
            });
    }
</script>
@endsection
