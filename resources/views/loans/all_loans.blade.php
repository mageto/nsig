@extends('layouts.app')

@section('content')

<div class="row mb-5">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                Loan Type Management
            <button style="float:right !important;" type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addNewMemberModal">
                Add New Loan Type
            </button>
            </div>
            <br>
            <div class="table-responsive">
              <table id="datatable-1" class="table table-datatable table-striped table-hover dataTable no-footer" role="grid" aria-describedby="datatable-1_info">
                  <thead class="thead-light">
                      <tr>
                        <th>#</th>
                          <th>Loan Type</th>
                          <th>Interest %</th>
                          <th>Description</th>
                          <th class="text-right">Action</th>
                      </tr>
                  </thead>
                  <tbody></tbody>
								</table>

                <script type="text/javascript">
                  var table;

                  $(document).ready(function() {
                  table = $('#datatable-1').DataTable({
                      processing: true,
                      serverSide: true,
                      responsive: true,
                      ajax: '{{ url("/show-loan-types") }}',
                      columns: [
                      { data: 'id',             name: 'id' ,              searchable: false},
                      { data: 'name',           name: 'name' ,            searchable: true},
                      { data: 'interest',       name: 'interest' ,        searchable: true},
                      { data: 'description',    name: 'description' ,     searchable: true},
                      { data: "action" }
                      ],
                  });
                  });
              </script>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addNewMemberModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Loan Type</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="name">Loan Type Name</label>
                                <input class="form-control form-control-sm" name="name" id="name" type="text" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <label for="interest">Percentage Interest</label>
                            <input type="text" class="form-control form-control-sm" name="interest" id="interest" placeholder="%">
                        </div>
                        <div class="form-group">
                            <label for="processing_fee">Processing Fee</label>
                            <input class="form-control form-control-sm" id="processing_fee" name="processing_fee" type="text" placeholder="Processing Fee">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control form-control-sm" id="description" name="description" type="text" placeholder="Description"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                    <button type="button" onclick="add_loantype()" id="btn-save" class="btn btn-primary btn-sm">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function edit_member(member_id){

        $.get('/get-member/' + member_id, function (data) {

            $('#btn-save').val("Update");
            console.log(data);
            $('#member_id').val(data[0].id);
            $('#name').val(data[0].name);
            $('#email').val(data[0].email);
            $('#gender').val(data[0].gender).change();
            $('#idNumber').val(data[0].idNumber);
            $('#phone').val(data[0].phone);
            $('#dob').val(data[0].dob).change();
          });
        }
    function add_loantype(){
      var name              =  document.getElementById('name').value;
      var interest          =  document.getElementById('interest').value;
      var processing_fee    =  document.getElementById('processing_fee').value;
      var description       =  document.getElementById('description').value;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
                }
            });

            $.ajax({
                url: '/add-loan-type', // point to server-side PHP script
                data: { name : name, interest : interest, processing_fee : processing_fee, description : description } ,
                type: 'POST',
                success: function(data) {
                    if (data == 1) {
                        toastr.error('ID Number Already Exists');
                    }else if (data == 2) {
                        toastr.error('Email Already Exists');
                    } else {
                        document.getElementById('name').val = "";
                        document.getElementById('interest').val = "";
                        document.getElementById('processing_fee').val = "";
                        document.getElementById('description').val = "";
                        table.ajax.reload(null,false); //reload datatable ajax
                        $('#addNewMemberModal').modal('hide');
                        toastr.success('Loan Type was added successfully');
                    }

                }
            });
    }
</script>
@endsection
