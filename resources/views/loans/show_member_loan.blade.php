@extends('layouts.app')

@section('content')

<div class="row mb-5">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                Guarantors : {{$approved}}
                <?php if (Auth::user()->memberType == 'admin' || Auth::user()->memberType == 'treasurer') { ?>
                    <button style="float:right !important;" type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#declineLoan">
                        Deny Loan
                    </button>
                    <button style="float:right !important;" type="button" class="btn btn-sm btn-primary mr-5" onclick="approve_loan({{$loanId}})">
                        Approve Loan
                    </button>
                <?php } ?>
            </div>
            <br>
            <div class="table-responsive">
              <table id="datatable-1" class="table table-datatable table-striped table-hover dataTable no-footer" role="grid" aria-describedby="datatable-1_info">
                    <thead class="thead-light">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Membership Number</th>
                            <th>Amount</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                  <tbody>
                      <?php foreach ($guarantors as $value) { ?>
                        <tr>
                            <td>{{ $value }}</td>
                            <td>{{ DB::table('users')
                                 ->where('users.id', $value)
                                 ->value('users.name') }}</td>
                            <td>{{ DB::table('users')
                                 ->where('users.id', $value)
                                 ->value('users.membershipNo') }}</td>
                            <td>{{ DB::table('guarantorship')
                                 ->where('guarantorship.loanId', $loanId)
                                 ->where('guarantorship.guarantorid', $value)
                                 ->value('guarantorship.amount') }}</td>
                            <td>{{ DB::table('guarantorship')
                                 ->where('guarantorship.loanId', $loanId)
                                 ->where('guarantorship.guarantorid', $value)
                                 ->value('guarantorship.status') }}</td>
                        </tr>
                      <?php } ?>
                        <tr>
                            
                            <td><b>#</b></td>
                            <td><b>Loan Request Amount</b></td>
                            <td><b><u>{{ DB::table('loans')
                                 ->where('id', $loanId)
                                 ->value('amount') }}</u></b>
                            </td>
                            <td><b>Loan Amount Guaranteed</b></td>
                            <td><b><u>{{ DB::table('users')
                                 ->join('guarantorship', 'guarantorship.memberId', '=', 'users.id')
                                 ->where('loanId', $loanId)
                                 ->sum('guarantorship.amount') }}</u></b>
                            </td>
                            
                        </tr>
                  </tbody>
				</table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="declineLoan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Deny Loan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <input type="hidden" value="{{$loanId}}" id="loan_id" name="loan_id">
                            <label for="comment">Comment</label>
                            <textarea class="form-control form-control-sm" id="comment" name="comment" type="text" placeholder="Comment"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                    <button type="button" onclick="deny_loan()" id="btn-save" class="btn btn-primary btn-sm">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function approve_loan(loan_id){
        var status = "Approved";

        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            }
        });

        $.ajax({
            url: '/approve-loan/' + loan_id, // point to server-side PHP script
            data: { status : status } ,
            type: 'POST',
            success: function(data) {
                if (data == 1) {
                    toastr.error('Could not approve Loan, there is a pending/denied gaurantor.');
                } else {
                    toastr.success('Loan Approved successfully');
                }
            }
        });

    }
        
    function deny_loan(){
        var comment       =  document.getElementById('comment').value;
        var loan_id       =  document.getElementById('loan_id').value;

        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            }
        });

        $.ajax({
            url: '/deny-loan/' + loan_id, // point to server-side PHP script
            data: { comment : comment } ,
            type: 'POST',
            success: function(data) {
                document.getElementById('comment').val = "";
                $('#declineLoan').modal('hide');
                toastr.success('Loan Denied successfully');
            }
        });
    }
</script>
@endsection
