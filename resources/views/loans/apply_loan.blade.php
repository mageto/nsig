@extends('layouts.app')

@section('content')

<div class="row mb-5">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                Apply Loan
            <!-- <button style="float:right !important;" type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addNewMemberModal">
                Add New Loan Type
            </button> -->
            </div>
            <div class="col-md-6 col-sm-12 mb-5 mt-5">
                <form>
                    <div class="form-group">
                        <label for="name">Loan Type Name</label>
                        
                        <select class="select2 form-control" name="loan_type" id="loan_type" data-placeholder="Choose ...">
                            <option value="">Choose Loan Type</option>
                            <?php foreach ($loan_types as $value) { ?>
                                <option value="{{ $value->id }}">{{ $value->name }}</option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="loan_amount">Amount Requested</label>
                        <input type="text" class="form-control form-control-sm" name="loan_amount" id="loan_amount" placeholder="Amount Requested">
                    </div>
                    <div class="form-group">
                        <label for="startDate">Effective From</label>
                        <input class="form-control form-control-sm" id="startDate" name="startDate" type="date">
                    </div>
                    <div class="form-group">
                        <label for="expectedFinishDate">Effective To</label>
                        <input class="form-control form-control-sm" id="expectedFinishDate" name="expectedFinishDate" type="date">
                    </div>
                    <!-- <div class="form-group">
                        <label for="monthly">Amount Per Month</label>
                        <input type="text" class="form-control form-control-sm" name="monthly" id="monthly" placeholder="Amount">
                    </div> -->
                    <div class="form-group">
                        <label for="guarantors">Guarantors</label>
                        
                        <select class="select2 form-control select2-multiple" name="guarantors" id="guarantors" multiple="multiple" multiple data-placeholder="Choose ...">
                            <option value="">Choose Guarantors</option>
                            <?php foreach ($guarantors as $value) { ?>
                                <option value="{{ $value->id }}">{{ $value->name }}, {{ $value->phone }}</option>
                            <?php } ?>
                        </select>
                    </div>
                    <button style="float:right;" type="button" onclick="add_loan_application()" id="btn-save" class="btn btn-primary btn-sm">Submit</button>
                </form>
            </div>
            <hr>
            
            <div class="table-responsive">
              <table id="datatable-1" class="table table-datatable table-striped table-hover dataTable no-footer" role="grid" aria-describedby="datatable-1_info">
                  <thead class="thead-light">
                      <tr>
                        <th>#</th>
                          <th>Loan Type</th>
                          <th>Date Applied %</th>
                          <th>Status</th>
                          <th class="text-right">Action</th>
                      </tr>
                  </thead>
                  <tbody></tbody>
								</table>

        
            </div>
        </div>
    </div>

    <!-- Modal
    <div class="modal fade" id="addNewMemberModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Loan Type</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="name">Loan Type Name</label>
                            <input class="form-control form-control-sm" name="name" id="name" type="text" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <label for="interest">Percentage Interest</label>
                            <input type="text" class="form-control form-control-sm" name="interest" id="interest" placeholder="%">
                        </div>
                        <div class="form-group">
                            <label for="processing_fee">Processing Fee</label>
                            <input class="form-control form-control-sm" id="processing_fee" name="processing_fee" type="text" placeholder="Processing Fee">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control form-control-sm" id="description" name="description" type="text" placeholder="Description"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                    <button type="button" onclick="add_loan_application()" id="btn-save" class="btn btn-primary btn-sm">Submit</button>
                </div>
            </div>
        </div>
    </div> -->
</div>

<script>
var firstDate   =  document.getElementById('name').value;
var secondDate  =  document.getElementById('name').value;
calculateTotalMonthsDifference = function(firstDate, secondDate) {
        var fm = firstDate.getMonth();
        var fy = firstDate.getFullYear();
        var sm = secondDate.getMonth();
        var sy = secondDate.getFullYear();
        var months = Math.abs(((fy - sy) * 12) + fm - sm);
        var firstBefore = firstDate > secondDate;
        firstDate.setFullYear(sy);
        firstDate.setMonth(sm);
        firstBefore ? firstDate < secondDate ? months-- : "" : secondDate < firstDate ? months-- : "";
        return months;
}

function edit_member(member_id){

        $.get('/get-member/' + member_id, function (data) {

            $('#btn-save').val("Update");
            console.log(data);
            $('#member_id').val(data[0].id);
            $('#name').val(data[0].name);
            $('#email').val(data[0].email);
            $('#gender').val(data[0].gender).change();
            $('#idNumber').val(data[0].idNumber);
            $('#phone').val(data[0].phone);
            $('#dob').val(data[0].dob).change();
          });
        }
    function add_loan_application(){
        var loan_type              =  document.getElementById('loan_type').value;
        var loan_amount          =  document.getElementById('loan_amount').value;
        var startDate    =  document.getElementById('startDate').value;
        var expectedFinishDate       =  document.getElementById('expectedFinishDate').value;
        var guarantors       =  $("#guarantors").val();
        // document.getElementById('guarantors').val;
    
        // console.log(guarantors);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
                }
            });

            $.ajax({
                url: '/add-loan-application', // point to server-side PHP script
                data: { loan_type : loan_type, loan_amount : loan_amount, startDate : startDate, expectedFinishDate : expectedFinishDate, guarantors : guarantors } ,
                type: 'POST',
                success: function(data) {
                    if (data == 1) {
                        toastr.error('You already have a Loan.');
                    }else if (data == 2) {
                        toastr.error('You have not qualified for a Loan.');
                    } else {
                        document.getElementById('name').val = "";
                        document.getElementById('interest').val = "";
                        document.getElementById('processing_fee').val = "";
                        document.getElementById('description').val = "";
                        table.ajax.reload(null,false); //reload datatable ajax
                        $('#addNewMemberModal').modal('hide');
                        toastr.success('Loan Type was added successfully');
                    }

                }
            });
    }
</script>
@endsection
