@extends('layouts.app')

@section('content')

<div class="row mb-5">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                Guarantor Requests
                    
            </div>
            <br>
            <div class="table-responsive">
              <table id="datatable-1" class="table table-datatable table-striped table-hover dataTable no-footer" role="grid" aria-describedby="datatable-1_info">
                    <thead class="thead-light">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Loan Amount</th>
                            <th>Phone</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                  <tbody>
                      <?php foreach ($loan_requests as $value) { ?>
                        <tr>
                            <td>{{ $value->id }}</td>
                            <td>{{ $value->name }}</td>
                            <td>{{ $value->amount }}</td>
                            <td>{{ $value->phone }}</td>
                            <td>
                                <button onclick="decline({{ $value->loan_id }})" type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#declineLoan">Deny</button>
                                <button onclick="approve({{ $value->loan_id }})" type="button" class="btn btn-sm btn-primary mr-5" data-toggle="modal" data-target="#approveLoan">Approved</button>
                            </td>
                        </tr>
                      <?php } ?>
                  </tbody>
				</table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="declineLoan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Deny Loan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <input type="hidden" value="" id="deny_loan_id" name="deny_loan_id">
                            <label for="comment">Reason</label>
                            <textarea class="form-control form-control-sm" id="comment" name="comment" type="text" placeholder="Reason"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                    <button type="button" onclick="deny_loan()" class="btn btn-primary btn-sm">Submit</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="approveLoan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Approve Loan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <input type="hidden" value="" id="approve_loan_id" name="approve_loan_id">
                            <label for="amount">Amount</label>
                            <input class="form-control form-control-sm" id="amount" name="amount" type="text" placeholder="Amount">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                    <button type="button" onclick="approve_loan()" id="btn-save" class="btn btn-primary btn-sm">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function approve(loan_id){
        document.getElementById('approve_loan_id').value = loan_id;
    }

    function decline(loan_id){
        document.getElementById('deny_loan_id').value = loan_id;
    }

    function approve_loan(){
        var amount       =  document.getElementById('amount').value;
        var approve_loan_id      =  document.getElementById('approve_loan_id').value;
        var status       =  "Approved";

        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            }
        });

        $.ajax({
            url: '/guarantor-approval', // point to server-side PHP script
            data: { status : status, amount : amount, approve_loan_id : approve_loan_id } ,
            type: 'POST',
            success: function(data) {
                if (data == 2) {
                    toastr.error('Could not approve guarantorship, you already have a loan.');
                } else if(data == 3){
                    toastr.error('Could not approve guarantorship, you already have guaranteed 3 people.');
                } else if(data == 4){
                    toastr.error('Could not approve guarantorship, you have insufficient funds.');
                } else {
                    toastr.success('Loan Guarantorship Approved successfully');
                }
            }
        });

    }
        
    function deny_loan(){
        var comment       =  document.getElementById('comment').value;
        var deny_loan_id       =  document.getElementById('deny_loan_id').value;
        var status       =  "Denied";

        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            }
        });

        $.ajax({
            url: '/guarantor-denial', // point to server-side PHP script
            data: { comment : comment, status : status, deny_loan_id : deny_loan_id } ,
            type: 'POST',
            success: function(data) {
                document.getElementById('comment').val = "";
                $('#declineLoan').modal('hide');
                toastr.success('Loan Denied successfully');
            }
        });
    }
</script>
@endsection
