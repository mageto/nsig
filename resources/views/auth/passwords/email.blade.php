<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from base5builder.com/livedemo/quillpro/v1.6/sisu-signin.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 Jul 2018 09:30:27 GMT -->
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="{{ asset('assets/img/logo.png') }}">

	<title>NSIG</title>

	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700&amp;subset=latin-ext" rel="stylesheet">

	<!-- CSS - REQUIRED - START -->
	<!-- Batch Icons -->
	<link rel="stylesheet" href="{{ asset('assets/fonts/batch-icons/css/batch-icons.css') }}">
	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap/bootstrap.min.css') }}">
	<!-- Material Design Bootstrap -->
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap/mdb.min.css') }}">
	<!-- Custom Scrollbar -->
	<link rel="stylesheet" href="{{ asset('assets/plugins/custom-scrollbar/jquery.mCustomScrollbar.min.css') }}">
	<!-- Hamburger Menu -->
	<link rel="stylesheet" href="{{ asset('assets/css/hamburgers/hamburgers.css') }}">

	<!-- CSS - REQUIRED - END -->

	<!-- CSS - OPTIONAL - START -->
	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{ asset('assets/fonts/font-awesome/css/font-awesome.min.css') }}">

	<!-- CSS - DEMO - START -->
	<link rel="stylesheet" href="{{ asset('assets/demo/css/ui-icons-batch-icons.css') }}">
	<!-- CSS - DEMO - END -->

	<!-- CSS - OPTIONAL - END -->

	<!-- QuillPro Styles -->
	<link rel="stylesheet" href="{{ asset('assets/css/quillpro/quillpro.css') }}">
</head>

<body>

<div class="container">
    <div class="row">
        <div style="margin-top:80px; margin-left:280px;" class="col-md-6 col-md-offset-2">
            <div class="panel panel-default">
            <div class="card">
				<div class="card-header">
                    Reset Password
				</div>
				<div class="card-body">
									
                <!-- <div class="panel-heading"></div> -->

                <!-- <div class="panel-body"> -->
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-12 col-xs-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
								</div>
							</div>
                <!-- </div> -->
            </div>
        </div>
    </div>
</div>
	<!-- SCRIPTS - REQUIRED START -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- Bootstrap core JavaScript -->
	<!-- JQuery -->
	<script type="text/javascript" src="{{ asset('assets/js/jquery/jquery-3.1.1.min.js') }}"></script>
	<!-- Popper.js - Bootstrap tooltips -->
	<script type="text/javascript" src="{{ asset('assets/js/bootstrap/popper.min.js') }}"></script>
	<!-- Bootstrap core JavaScript -->
	<script type="text/javascript" src="{{ asset('assets/js/bootstrap/bootstrap.min.js') }}"></script>
	<!-- MDB core JavaScript -->
	<script type="text/javascript" src="{{ asset('assets/js/bootstrap/mdb.min.js') }}"></script>
	<!-- Velocity -->
	<script type="text/javascript" src="{{ asset('assets/plugins/velocity/velocity.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/plugins/velocity/velocity.ui.min.js') }}"></script>
	<!-- Custom Scrollbar -->
	<script type="text/javascript" src="{{ asset('assets/plugins/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
	<!-- jQuery Visible -->
	<script type="text/javascript" src="{{ asset('assets/plugins/jquery_visible/jquery.visible.min.js') }}"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script type="text/javascript" src="{{ asset('assets/js/misc/ie10-viewport-bug-workaround.js') }}"></script>

	<!-- SCRIPTS - REQUIRED END -->

	<!-- SCRIPTS - OPTIONAL START -->
	<!-- Image Placeholder -->
	<script type="text/javascript" src="{{ asset('assets/js/misc/holder.min.js') }}"></script>
	<!-- SCRIPTS - OPTIONAL END -->

	<!-- QuillPro Scripts -->
	<script type="text/javascript" src="{{ asset('assets/js/scripts.js') }}"></script>
</body>

<!-- Mirrored from base5builder.com/livedemo/quillpro/v1.6/sisu-signin.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 Jul 2018 09:30:27 GMT -->
</html>
