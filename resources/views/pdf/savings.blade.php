<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Savings</title>
    <link rel="stylesheet" href="style.css" media="all" />
		<style rel="stylesheet" media="all" >
		@font-face {
			font-family: SourceSansPro;
			src: url(SourceSansPro-Regular.ttf);
		}

		.clearfix:after {
			content: "";
			display: table;
			clear: both;
		}

		a {
			color: #214ca1;
			text-decoration: none;
		}

		body {
			position: relative;
			width: 21cm;
			height: 29.7cm;
			margin: 0 auto;
			color: #555555;
			background: #FFFFFF;
			font-family: Arial, sans-serif;
			font-size: 14px;
			font-family: SourceSansPro;
		}

		header {
			padding: 10px 0;
			margin-bottom: 20px;
			border-bottom: 1px solid #AAAAAA;
		}

		#logo {
			float: left;
			margin-top: 8px;
		}

		#logo img {
			height: 70px;
		}

		#company {
			/* float: right; */
			margin-right: 50px;
			text-align: right;
		}


		#details {
			margin-bottom: 50px;
		}

		#client {
			padding-left: 6px;
			border-left: 6px solid #214ca1;
			float: left;
		}

		#client .to {
			color: #777777;
		}

		h2.name {
			font-size: 1.4em;
			font-weight: normal;
			margin: 0;
		}

		#invoice {
			/* float: right; */
			margin-right: 50px;
			text-align: right;
		}

		#invoice h1 {
			color: #214ca1;
			font-size: 2.4em;
			line-height: 1em;
			font-weight: normal;
			margin: 0  0 10px 0;
		}

		#invoice .date {
			font-size: 1.1em;
			color: #777777;
		}

		table {
			width: 93%;
			border-collapse: collapse;
			border-spacing: 0;
			margin-bottom: 20px;
		}

		table th,
		table td {
			padding: 10px;
			background: #EEEEEE;
			text-align: left;
			border-bottom: 1px solid #FFFFFF;
		}

		table th {
			white-space: nowrap;
			font-weight: normal;
		}

		table td {
			text-align: right;
		}

		table td h3{
			color: #92278f;
			font-size: 1.2em;
			font-weight: normal;
			margin: 0 0 0.0 0;
		}

		table .no {
			color: #FFFFFF;
			font-size: 1.6em;
			background: #92278f;
		}

		table .desc {
			text-align: left;
		}

		table .unit {
			background: #DDDDDD;
		}

		table .qty {
		}

		table .total {
			background: #92278f;
			color: #FFFFFF;
		}

		table td.unit,
		table td.qty,
		table td.total {
			font-size: 1.2em;
		}

		table tbody tr:last-child td {
			border: none;
		}

		table tfoot td {
			padding: 5px 10px;
			background: #FFFFFF;
			border-bottom: none;
			font-size: 1.2em;
			white-space: nowrap;
			border-top: 1px solid #AAAAAA;
		}

		table tfoot tr:first-child td {
			border-top: none;
		}

		table tfoot tr:last-child td {
			color: #92278f;
			font-size: 1.4em;
			border-top: 1px solid #92278f;

		}

		table tfoot tr td:first-child {
			border: none;
		}

		#thanks{
			font-size: 2em;
			margin-bottom: 50px;
		}

		#notices{
			padding-left: 6px;
			border-left: 6px solid #214ca1;
		}

		#notices .notice {
			font-size: 1.2em;
		}

		footer {
			color: #777777;
			width: 100%;
			height: 30px;
			position: absolute;
			bottom: 0;
			border-top: 1px solid #AAAAAA;
			padding: 8px 0;
			text-align: center;
		}


		</style>
  </head>
  <body>
    <header class="clearfix">
      <div id="logo">
        <img src="http://nsig.co.ke/images/logo.png">
      </div>
      <div id="company">
        <h2 class="name">Neptune Investments Group</h2>
        <div>+254-724274848</div>
        <div><a href="mailto:info@nsig.co.ke">info@nsig.co.ke</a></div>
      </div>
      </div>
    </header>
		<?php foreach ($member as $value) { ?>
    <main>
      <div id="details" class="clearfix">
        <div id="client">
          <div class="to">SEND TO:</div>
          <h2 class="name">{{ $value->name }}:{{ $value->membershipNo }}</h2>
          <div class="address">ID: {{ $value->idNumber }}</div>
          <div class="email"><a href="mailto:{{ $value->email }}">{{ $value->email }}</a></div>
        </div>
        <div id="invoice">
          <h1>RECEIPT {{ $value->receiptNo }}</h1>
          <div class="date">Transaction Ref: {{ $value->transactionRef }}</div>
          <div class="date">Date: {{ $value->datePaid }}</div>
        </div>
      </div>
      <table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th class="no">#</th>
						<th class="desc">PAYMENT</th>
            <th class="total">AMOUNT</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="no">1</td>
            <td class="desc"><h3>Savings</td>
            <td class="total">Ksh {{ number_format($value->savings) }}</td>
          </tr>
					<?php if ($value->shares > 1) { ?>
						<tr>
							<td class="no">2</td>
							<td class="desc"><h3>Shares</td>
							<td class="total">Ksh {{ number_format($value->shares) }}</td>
						</tr>
					<?php if ($value->loan > 1) { ?>
          <tr>
            <td class="no">3</td>
            <td class="desc"><h3>Loan</td>
            <td class="total">Ksh {{ number_format($value->loan) }}</td>
          </tr>
          <tr>
            <td class="no">4</td>
            <td class="desc"><h3>Interest</td>
            <td class="total">Ksh {{ number_format($value->interest) }}</td>
          </tr>
						<?php if ($value->fine > 1) { ?>
							<tr>
								<td class="no">5</td>
								<td class="desc"><h3>Fine</td>
								<td class="total">Ksh {{ number_format($value->fine) }}</td>
							</tr>
						<?php } ?>
					<?php } elseif ($value->fine > 1) { ?>
          <tr>
            <td class="no">3</td>
            <td class="desc"><h3>Fine</td>
            <td class="total">Ksh {{ number_format($value->fine) }}</td>
          </tr>
					<?php } ?>
					<?php } else { ?>
					<?php if ($value->loan > 1) { ?>
          <tr>
            <td class="no">2</td>
            <td class="desc"><h3>Loan</td>
            <td class="total">Ksh {{ number_format($value->loan) }}</td>
          </tr>
          <tr>
            <td class="no">3</td>
            <td class="desc"><h3>Interest</td>
            <td class="total">Ksh {{ number_format($value->interest) }}</td>
          </tr>
						<?php if ($value->fine > 1) { ?>
							<tr>
								<td class="no">4</td>
								<td class="desc"><h3>Fine</td>
								<td class="total">Ksh {{ number_format($value->fine) }}</td>
							</tr>
						<?php } ?>
					<?php } elseif ($value->fine > 1) { ?>
          <tr>
            <td class="no">2</td>
            <td class="desc"><h3>Fine</td>
            <td class="total">Ksh {{ number_format($value->fine) }}</td>
          </tr>
					<?php } ?>
					<?php } ?>
        </tbody>
        <tfoot>
          <tr>
            <td></td>
            <td >GRAND TOTAL</td>
            <td>Ksh {{ $value->total_amount }}</td>
          </tr>
        </tfoot>
      </table>
      <div id="thanks">Thank you!</div>
      <div id="notices">
        <div>NOTICE:</div>
        <div class="notice">For any issues please call on 0715823203 or email us on info@nsig.co.ke</div>
      </div>
    </main>

	<?php } ?>
    <footer>
      Receipt was created on a computer and is valid without the signature and stamp.
    </footer>
  </body>
</html>
