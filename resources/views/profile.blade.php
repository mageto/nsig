@extends('layouts.app')

@section('content')
<main class="main-content p-5" role="main">
					<div class="row mb-4">
						<div class="col-md-12">
							<div class="card">
								<!-- <img class="card-img" src="assets/img/bg-image-1-horizontal.jpg" alt="Feature Image"> -->
								<div class="card-user-profile">
									<div class="profile-page-left">
										<div class="row">
											<div class="col-lg-12 mb-4">
												<div class="profile-picture profile-picture-lg bg-gradient bg-primary mb-4">
													<img src="avatar/{{ Auth::user()->avatar }}" width="144" height="144">
												</div>
											</div>
										</div>
										<hr />
										<div style="border:1px solid #A4A4A4; border-radius:5px;">
											<h4>Upload ID and Passport Photo</h4>
											<hr>
											<form style="padding:10px;"  enctype="multipart/form-data" method="POST" action="{{ url('profile/passport')}}">

												<input type="hidden" value="{{csrf_token()}}" name="_token" />
												<div class="form-group">
													<label for="passport">Passport Photo</label>
													<input type="file" class="form-control" name="passport" id="passport" placeholder="Passport Photo" value="" />
												</div>
												<div class="form-group">
													<label for="nationalID">National ID</label>
													<input type="file" class="form-control" name="nationalID" id="nationalID" placeholder="National ID" value="" />
												</div>
											<input type="hidden" name="member_id" id="member_id" value="{{ Auth::user()->id }}" />
												<button type="submit" class="btn btn-primary btn-sm">Submit</button>
											</form>
										</div>
									</div>
									<div class="profile-page-center">
										<?php if (Auth::user()->memberType == 'admin') { ?>
											<h1 class="card-user-profile-name">{{ Auth::user()->name }}</h1>
										<?php } else { ?>
											<h1 class="card-user-profile-name">{{ Auth::user()->name }}:{{ Auth::user()->membershipNo }}</h1>
										<?php } ?>

										<hr />
                    <div class="col-lg-6">
                      <form  enctype="multipart/form-data" method="POST" action="{{ url('profile/create')}}">

                          <input type="hidden" value="{{csrf_token()}}" name="_token" />
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input class="form-control" name="name" id="name" value="{{ Auth::user()->name }}" placeholder="Name"/>
                            </div>
                            <div class="form-group">
                                  <label for="email">Email</label>
                                  <input type="email" class="form-control" name="email" id="email" placeholder="Email"  value="{{ Auth::user()->email }}" />
                              </div>
                            <div class="form-group">
                                <label for="phone">Mobile</label>
                                <input type="text" class="form-control" name="phone" id="phone" placeholder="Mobile"  value="{{ Auth::user()->phone }}" />
                            </div>
                            <div class="form-group">
                                <label for="password">password</label>
                                <input type="password" class="form-control" name="password" id="password" autocomplete="new-password"/>
                            </div>
                            <div class="form-group">
                                <label for="idNumber">ID Number</label>
                                <input type="text" class="form-control" name="idNumber" id="idNumber" placeholder="ID Number" value="{{ Auth::user()->idNumber }}" />
                            </div>
                            <div class="form-group">
                                <label for="dob">D.O.B</label>
      													<input class="form-control form-control-sm" id="dob" name="dob" type="date" value="{{Auth::user()->dob }}">
      											</div>
                            <hr>
                            <div class="form-group">
                              <label for="avatar">Profile Pic</label>
                              <input type="file" class="form-control" name="avatar" id="avatar" placeholder="Profile Pic" value="" />
                            </div>
                          <input type="hidden" name="member_id" id="member_id" value="{{ Auth::user()->id }}" />
                          <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                          <a style="float:right;" href="" class="btn btn-danger btn-sm">Cancel</a>
                      </form>
                    </div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</main>
@endsection
