<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Neptune Investments Group - Neptune Investments Group. The Self Help Group to join.</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('landing/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/css/et-line-icons.css') }}"/>
    <link rel="stylesheet" href="{{ asset('landing/css/font-awesome.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('landing/css/iconmonstr-iconic-font.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('landing/css/lity.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('landing/css/animate.css') }}"/>
    <link rel="stylesheet" href="{{ asset('landing/css/owl.carousel.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('landing/css/owl.theme.default.min.css') }}"/>
	<link rel="stylesheet" href="{{ asset('landing/css/main.css') }}">
	<link rel="stylesheet" href="{{ asset('landing/css/responsive.css') }}">
	<link href="https://fonts.googleapis.com/css?family=Fira+Sans:100,200,300,400,500,600,700,800%7cPoppins:100,200,300,400,500,600,700,800" rel="stylesheet">
  </head>
  <body>
	  <!-- ==================================================
							load-wrapp
      ================================================== -->
	  <div class="load-wrapp">
		  <div class="wrap">
			  <ul class="dots-box">
			    <li class="dot"><span></span></li>
			    <li class="dot"><span></span></li>
			    <li class="dot"><span></span></li>
			    <li class="dot"><span></span></li>
			    <li class="dot"><span></span></li>
			  </ul>
		  </div>
      </div> 
	  <!-- ==================================================
							End load-wrapp
      ================================================== -->
	  
	  <!-- ==================================================
							navbar
      ================================================== -->
	  <nav class="navbar navbar-transparent navbar-black-links navbar-expand-lg">
		  <div class="container">
			  <a class="navbar-brand" href="#"> 
				  <img src="{{ asset('landing/images/logo.png') }}" alt="logo">
			  </a>
			  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="fa fa-bars"></span>
			  </button>
			  <div class="collapse navbar-collapse" id="main-navbar">
				  <ul class="navbar-nav ml-auto">  
				      <li class="nav-item">
					  	<a class="nav-link" href="#" data-scroll-nav="1">Home</a>
				  	  </li>
					  <li class="nav-item">
					  	<a class="nav-link" href="#" data-scroll-nav="2">About</a>
					  </li>
					  <li class="nav-item">
					  	<a class="nav-link" href="#" data-scroll-nav="3">Services</a>
					  </li>
					  <li class="nav-item">
					  	<a class="nav-link" href="#" data-scroll-nav="4">Faqs</a>
					  </li>
					  <li class="nav-item">
					  	<a class="nav-link" href="#" data-scroll-nav="5">Downloads</a>
					  </li>
					  <!-- <li class="nav-item">
					  	<a class="nav-link" href="#" data-scroll-nav="5">Contact Us</a>
					  </li> -->
					  <li class="nav-item log-in">
					  	<a class="nav-link flex-center bg-blue radius-5px transition-3" href="login">Log In</a>
					  </li>
				  </ul>
			  </div>
		  </div>
	  </nav>
	  <!-- ==================================================
							End navbar
      ================================================== -->
	  
	  <!-- ==================================================
							welcome-area
      ================================================== -->
	  <section class="welcome-area sec-padding p-relative o-hidden" data-scroll-index="1">
		  <div class="container">
			  <div class="row welcome-text sec-padding flex-center">
				  <div class="col-md-6 mb-50px z-index-1">
					  <h1 class="mb-20px">NSIG</h1>
					  <p>We believe that the value of money is not obtained from simply banking, but engaging it in viable investment options. One can join NIG at any given time.</p>
					  <!-- <a class="main-btn btn-3 mt-30px" href="" data-lity><i class="fa fa-arrow-right"></i> About Us </a> -->
				  </div>
				  <div class="col-md-6 text-center">
					  <img style="border-radius: 10px;" alt="img" src="{{ asset('landing/images/landing-1.jpeg') }}">
				  </div>
			  </div>
		  </div>
		  <div class="shape-1 bg-gray p-absolute">
		  </div>
	  </section>
	  <!-- ==================================================
							End welcome-area
      ================================================== -->
	  
	  <!-- ==================================================
							about-area
      ================================================== -->
	  <section class="about-area sec-padding" data-scroll-index="2">
		  <div class="container">
			  <div class="row mb-25px"> 
				  <div class="col-md-6">
					  <div class="mt-25px mb-25px wow fadeInLeft" data-wow-delay="0.45s">
						  <img style="border-radius: 10px;" src="{{ asset('landing/images/landing-2.jpeg') }}" alt="img">
					  </div>
				  </div>
				  <div class="col-md-6">
					  <div class="mt-25px mb-25px wow fadeInRight" data-wow-delay="0.7s">
						  <span class="fs-20 fw-400 pt-10px pb-10px pr-25px pl-25px radius-50px bg-gray color-blue">Neptune Savings and Investments Group</span>
						  <h3 class="mb-15px mt-20px">Why Join Us</h3>
						  <p class="mb-15px">NIG is a Self Help GroupÂ registered under the Ministry of Labour and Social Protection and it was established in September 30th 2018.</p>
						  <p class="mb-15px">Neptune offers current and prospective investors with an opportunity to achieve financial freedom and grow wealth through various investment options. Membership is open to the public</p>
						  <p class="mb-10px">We believe that the value of money is not obtained from simply banking, but engaging it in viable investment options. One can join NIG at any given time.</p>
					  </div>
				  </div>    
			  </div>
			  <div class="row mb-25px"> 
				  <div class="col-md-6">
					  <div class="mt-25px mb-25px wow fadeInLeft" data-wow-delay="0.7s">
						  <span class="fs-20 fw-400 pt-10px pb-10px pr-25px pl-25px radius-50px bg-gray color-blue">Our Core Values</span>
						  <h3 class="mb-15px mt-20px">We are driven by the following values</h3>
						  <p class="mb-10px"><i class="fa fa-check color-blue mr-5px"></i>Teamwork</p>
						  <p class="mb-10px"><i class="fa fa-check color-blue mr-5px"></i>Innovation</p>
						  <p class="mb-10px"><i class="fa fa-check color-blue mr-5px"></i>Professionalism</p>
						  <p class="mb-10px"><i class="fa fa-check color-blue mr-5px"></i>Integrity</p>
						  <p class="mb-10px"><i class="fa fa-check color-blue mr-5px"></i>Equality</p>
					  </div>
				  </div> 
				  <div class="col-md-6">
					  <div class="mt-25px mb-25px wow fadeInRight" data-wow-delay="0.45s">
						  <img src="{{ asset('landing/images/landing-3.jpg') }}" alt="img">
					  </div>
				  </div>
			  </div>
			  <div class="row mb-25px"> 
				  <div class="col-md-6">
					  <div class="mt-25px mb-25px wow fadeInLeft" data-wow-delay="0.45s">
						  <img src="{{ asset('landing/images/objectives.jpg') }}" alt="img">
					  </div>
				  </div>
				  <div class="col-md-6">
					  <div class="mt-25px mb-25px wow fadeInRight" data-wow-delay="0.7s">
						  <span class="fs-20 fw-400 pt-10px pb-10px pr-25px pl-25px radius-50px bg-gray color-blue">Grow Your Finances</span>
						  <h3 class="mb-15px mt-20px">Our Objectives</h3>
						  <p class="mb-30px">Neptune Investments Group (NIG), established in September 2018, is a subsidiary of Neptune Investments Club (NIC), which was established in October 2013. Our main objectives are :</p>
						  <div class="row">
							  <div class="col-md-6">
								  <div class="mb-40px">
									  <i class="icon-tools-2 fs-30 mb-15px color-blue"></i>
									  <!-- <h5>Helping Support</h5> -->
									  <p>To empower our members financially through encouraging a saving culture.</p>
								  </div>
							  </div>
							  <div class="col-md-6">
								  <div class="">
									  <i class="icon-bike fs-30 mb-15px color-blue"></i>
									  <!-- <h5>Qualified Staff</h5> -->
									  <p>To pool membersâ€™ resources to invest as any opportunity arises.</p>
								  </div>
							  </div>
						  </div>
					  </div>
				  </div>    
			  </div>
		  </div>
	  </section>
	  <!-- ==================================================
							End about-area
      ================================================== -->
	  
	  <!-- ==================================================
      ================================================== -->
	  <section class="text-center bg-gray triangle-top triangle-bottom">
		  <div class="container">
			  <div class="row">
				  <div class="col-md-8 offset-md-2">
					  <div class="mt-25px ">
						  <h3 class="mb-15px wow fadeInUp" data-wow-delay="0.45s">We come <span class="color-blue">together </span> to discuss growth and investment opportunities.</h3>
						  <!-- <p class="mb-20px wow fadeInUp" data-wow-delay="0.5s">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incididunt labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi.</p> -->
						  <!-- <a class="main-btn btn-3 before-gray mb-30px wow fadeInUp" data-wow-delay="0.55s" href="#">Read More</a> -->
						  <img alt="img" src="{{ asset('landing/images/y.png') }}" class="d-block m-auto wow fadeInUp" data-wow-delay="0.55s">
					  </div>
				  </div>
			  </div>
		  </div>
	  </section>
	  <!-- ==================================================
      ================================================== -->
	  
	  <!-- ==================================================
							services-area-5
      ================================================== -->
	  <section class="services-area-5 sec-padding text-center p-relative" data-scroll-index="3">
		  <div class="container">
			  <h1 class="title-h">Our Services</h1>
			  <p class="title-p">We offer a wide range of investment options, ranging from financial services to property management.</p>
			  <div class="row">
				  <div class="col-md-3">
					  <div class="services-text pl-30px pr-30px mt-25px mb-25px wow fadeInUp" data-wow-delay="0.3s">
						  <i class="im im-umbrella p-absolute color-blue fs-35 bg-gray text-center radius-50 mb-25px transition-2"></i>
						  <div class="pl-">
							  <h4 class="mb-5px">Youths</h4>
							  <span class="color-blue bg-gray pl-15px pr-15px pt-5px pb-5px radius-50px fw-400 fs-14">Financial Freedom</span>
						  	  <p class="mt-15px">NIG offers guidance to the youth population on how to achieve financial freedom through seminars and education days.</p>
						  </div>
					  </div>
				  </div>
				  <div class="col-md-3">
					  <div class="services-text pl-30px pr-30px mt-25px mb-25px wow fadeInUp" data-wow-delay="0.5s">
						  <i class="im im-sun p-absolute color-blue fs-35 bg-gray text-center radius-50 mb-25px transition-2"></i>
						  <div class="pl-">
							  <h4 class="mb-5px">Investments</h4>
							  <span class="color-blue bg-gray pl-15px pr-15px pt-5px pb-5px radius-50px fw-400 fs-14">Investment Opportunities</span>
							  <p class="mt-15px">Through our wide network and partnerships, we seek viable investment ideas and opportunities for our members.</p>
						  </div>
					  </div>
				  </div>
				  <div class="col-md-3">
					  <div class="services-text pl-30px pr-30px mt-25px mb-25px wow fadeInUp" data-wow-delay="0.7s">
						  <i class="im im-book p-absolute color-blue fs-35 bg-gray text-center radius-50 mb-25px transition-2"></i>
						  <div class="pl-">
							  <h4 class="mb-5px">Education</h4>
							  <span class="color-blue bg-gray pl-15px pr-15px pt-5px pb-5px radius-50px fw-400 fs-14">Education Fairs</span>
							  <p class="mt-15px">NIG organizes education fairs and forums on matters Investments and personal development.</p>
						  </div>
					  </div>
				  </div>
				  <div class="col-md-3">
					  <div class="services-text pl-30px pr-30px mt-25px mb-25px wow fadeInUp" data-wow-delay="0.7s">
						  <i class="im im-bank p-absolute color-blue fs-35 bg-gray text-center radius-50 mb-25px transition-2"></i>
						  <div class="pl-">
							  <h4 class="mb-5px">Loans</h4>
							  <span class="color-blue bg-gray pl-15px pr-15px pt-5px pb-5px radius-50px fw-400 fs-14">Give Out Loans</span>
							  <p class="mt-15px">We offer both secured and unsecured loaning services to both our members and members of the public.</p>
						  </div>
					  </div>
				  </div>
			  </div>
		  </div>
	  </section>
	  <!-- ==================================================
							End services-area-5
      ================================================== -->
	  
	  <!-- ==================================================
							team-area-2
      ================================================== -->
	  <section class="team-area-2 bg-gray text-center triangle-top triangle-bottom">
		  <div class="container">
			  <h1 class="title-h">Our Leadership</h1>
			  <p class="title-p">Neptune Savings and Investments Group is led by : </p>
			  <div class="row">
				  <div class="col-md-1 col-sm-12 wow fadeInUp" data-wow-delay="0.3s"></div>
					<div class="col-md-2 col-sm-6 wow fadeInUp" data-wow-delay="0.3s">
					  <div class="mt-25px mb-25px">
						  <div class="p-relative o-hidden d-inline-block radius-10px mb-20px ml-auto mr-auto">
						  	  <img src="{{ asset('landing/images/leadership-1.jpg') }}" alt="img" class="radius-10px transition-3">
						  </div>
						  <h5 class="mb-0px">Simeon Nyachae</h5>
						  <span class="color-blue fs-15 d-block mb-15px">Chairperson</span>
					  </div>
				  </div>
				  <div class="col-md-2 col-sm-6 wow fadeInUp" data-wow-delay="0.5s">
					  <div class="mt-25px mb-25px">
						  <div class="p-relative o-hidden d-inline-block radius-10px mb-20px ml-auto mr-auto">
						  	  <img src="{{ asset('landing/images/leadership-2.jpg') }}" alt="img" class="radius-10px transition-3">
						  </div>
						  <h5 class="mb-0px">Richard Onyiego</h5>
						  <span class="color-blue fs-15 d-block mb-15px">Vice Chairperson</span>
					  </div>
				  </div>
				  <div class="col-md-2 col-sm-6 wow fadeInUp" data-wow-delay="0.7s">
					  <div class="mt-25px mb-25px">
						  <div class="p-relative o-hidden d-inline-block radius-10px mb-20px ml-auto mr-auto">
						  	  <img src="{{ asset('landing/images/leadership-3.jpg') }}" alt="img" class="radius-10px transition-3">
						  </div>
						  <h5 class="mb-0px">Susan Nyabate</h5>
						  <span class="color-blue fs-15 d-block mb-15px">Secretary</span>
					  </div>
				  </div>
				  <div class="col-md-2 col-sm-6 wow fadeInUp" data-wow-delay="0.9s">
					  <div class="mt-25px mb-25px">
						  <div class="p-relative o-hidden d-inline-block radius-10px mb-20px ml-auto mr-auto">
						  	  <img src="{{ asset('landing/images/leadership-4.jpg') }}" alt="img" class="radius-10px transition-3">
						  </div>
						  <h5 class="mb-0px">Winnie Moegi</h5>
						  <span class="color-blue fs-15 d-block mb-15px">Treasurer</span>
						  
					  </div>
				  </div>
				  <div class="col-md-2 col-sm-6 wow fadeInUp" data-wow-delay="0.9s">
					  <div class="mt-25px mb-25px">
						  <div class="p-relative o-hidden d-inline-block radius-10px mb-20px ml-auto mr-auto">
						  	  <img src="{{ asset('landing/images/leadership-5.jpg') }}" alt="img" class="radius-10px transition-3">
						  </div>
						  <h5 class="mb-0px">Sam Nyang'u</h5>
						  <span class="color-blue fs-15 d-block mb-15px">Investments Manager</span>
						  
					  </div>
				  </div>
			  </div>
		  </div>
	  </section>
	  <!-- ==================================================
							End team-area-2
      ================================================== -->
	  
	  <!-- ==================================================
							vision-area
      ================================================== -->
	  <section class="vision-area sec-padding">
		  <div class="container">
			  <div class="row">
				  <div class="col-md-5 mt-25px mb-25px">
					  <div class="vision-dots">
						  <div class="dot mb-40px">
							  <i class="im im-idea fs-20 bg-gray radius-50 text-center mb-10px mr-5px"></i> 
							  <h4 class="mb-5px">Our Mission</h4>
							  <p>To empower members financially through pooling together of financial resources.</p>
						  </div>
						  <div class="dot mb-40px">
							  <!-- <i class="im im-idea fs-20 bg-gray radius-50 text-center mb-10px mr-5px"></i>  -->
							  <h4 class="mb-5px">Our Vision</h4>
							  <p>To be a dynamic savings and Investments space accessible by everyone.</p>
						  </div>
						  <div class="dot">
							  <!-- <i class="im im-laptop-o fs-20 bg-gray radius-50 text-center mb-10px mr-5px"></i>  -->
							  <h4 class="mb-5px">Our Objectives</h4>
							  <p>
								  <ol>
									  <li>To empower members financially through encouraging a savings culture.</li>
									  <li>To members resources to invest as any opportunity arises.</li>
									  <li>To translate members contributions into profitable investments.</li>
								  </ol>
							  </p>
						  </div>
					  </div>
				  </div>
				  <div class="col-md-6 offset-md-1 mt-60px">
					  <div class="owl-carousel">
						  <div>
							  <img alt="img" src="{{ asset('landing/images/me-1.jpg') }}">
						  </div>
						  <div>
							  <img alt="img" src="{{ asset('landing/images/me-1.jpg') }}">
						  </div>
						  <div>
							  <img alt="img" src="{{ asset('landing/images/me-1.jpg') }}">
						  </div>
					  </div>
				  </div>
			  </div>  
		  </div>
	  </section>
	  <!-- ==================================================
							End vision-area
      ================================================== -->
	 
	  <!-- ==================================================
							how-work
      ================================================== -->
	  <!-- <section class="how-work text-center sec-padding">
		  <div class="container">
			  <h1 class="title-h">How It Works</h1>
			  <p class="title-p">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
			  <div class="row">
				  <div class="col-md-4 p-relative">
					  <div class="mt-25px mb-25px pr-30px pl-30px">
						  <span class="p-relative d-inline-block bg-gray color-blue radius-50 text-center fs-30 fw-600 mb-15px transition-3">
							  01
						  </span>
						  <h4>Design</h4>
						  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
					  </div>
				  </div>
				  <div class="col-md-4 p-relative">
					  <div class="mt-25px mb-25px pr-30px pl-30px">
						  <span class="p-relative d-inline-block bg-gray color-blue radius-50 text-center fs-30 fw-600 mb-15px transition-3">
							  02
						  </span>
						  <h4>Develop</h4>
						  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</p>
					  </div>
				  </div>
				  <div class="col-md-4 p-relative last-one">
					  <div class="mt-25px mb-25px pr-30px pl-30px">
						  <span class="p-relative d-inline-block bg-gray color-blue radius-50 text-center fs-30 fw-600 mb-15px transition-3">
							  03
						  </span>
						  <h4>Product</h4>
						  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</p>
					  </div>
				  </div>
			  </div>
		  </div>
	  </section> -->
	  <!-- ==================================================
							End how-work
      ================================================== -->
	  
	  <!-- ==================================================
							testimonials-2
      ================================================== -->
	  <section class="testimonials-2 bg-gray triangle-top triangle-bottom">
		  <div class="container">
			  <!-- <h1 class="title-h">Clients Review</h1> -->
			  <!-- <p class="title-p">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> -->
			  <div class="owl-carousel owl-theme pr-50px pl-50px">
				  <div class="single-review">
					  <div class="row justify-content-md-center bg-fff p-50px">
						  <!-- <div class="col-md-3">
							  <img alt="img" src="{{ asset('landing/images/team-5.jpg') }}" class="radius-50 ml-auto mr-auto mb-20px">
						  </div> -->
						  <div class="col-md-8">
							  <p class="fs-16 fw-300 color-555">Great investors need to have the right combination of intuition, business sense and investment talent.</p>
							  <h5 class="mt-20px mb-0px">Andrew Lo </h5>
							  <!-- <span class="color-blue fw-400 fs-15 d-block mb-10px">Web Designer</span> -->
							  <p>
								  <i class="fa fa-star color-gold"></i> 
								  <i class="fa fa-star color-gold"></i> 
								  <i class="fa fa-star color-gold"></i> 
								  <i class="fa fa-star color-gold"></i> 
								  <i class="fa fa-star color-gold"></i> 
							  </p>
						  </div>
					  </div>
				  </div>
				  <!-- <div class="single-review">
					  <div class="row justify-content-md-center bg-fff p-50px">
						  <div class="col-md-3">
							  <img alt="img" src="{{ asset('landing/images/team-6.jpg') }}" class="radius-50 ml-auto mr-auto mb-20px">
						  </div>
						  <div class="col-md-8">
							  <p class="fs-16 fw-300 color-555">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incididunt labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi.</p>
							  <h5 class="mt-20px mb-0px">Robert Richard</h5>
							  <span class="color-blue fw-400 fs-15 d-block mb-10px">App Developer</span>
							  <p>
								  <i class="fa fa-star color-gold"></i> 
								  <i class="fa fa-star color-gold"></i> 
								  <i class="fa fa-star color-gold"></i> 
								  <i class="fa fa-star color-gold"></i> 
								  <i class="fa fa-star-half-o color-gold"></i> 
							  </p>
						  </div>
					  </div>
				  </div>
				  <div class="single-review">
					  <div class="row justify-content-md-center bg-fff p-50px">
						  <div class="col-md-3">
							  <img alt="img" src="{{ asset('landing/images/team-1.jpg') }}" class="radius-50 ml-auto mr-auto mb-20px">
						  </div>
						  <div class="col-md-8">
							  <p class="fs-16 fw-300 color-555">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incididunt labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi.</p>
							  <h5 class="mt-20px mb-0px">Robert Edward</h5>
							  <span class="color-blue fw-400 fs-15 d-block mb-10px">App Developer</span>
							  <p>
								  <i class="fa fa-star color-gold"></i> 
								  <i class="fa fa-star color-gold"></i> 
								  <i class="fa fa-star color-gold"></i> 
								  <i class="fa fa-star color-gold"></i> 
								  <i class="fa fa-star-o color-gold"></i> 
							  </p>
						  </div>
					  </div>
				  </div> -->
			  </div>
		  </div>
	  </section>
	  <!-- ==================================================
							End testimonials-2
      ================================================== -->
	  
	  <!-- ==================================================
							faqs
      ================================================== -->
	  <section class="sec-padding faqs" data-scroll-index="4">
		  <div class="container">
			  <h1 class="title-h">Frequently Asked Question</h1>
			  <p class="title-p">Can't find your answers here? Drop us an email: <b class="color-blue">info@nsig.co.ke</b></p>
			  <div class="row">
				  <div class="col-md-6">
					  <div class="box mt-25px mb-25px wow fadeInLeft" data-wow-delay="0.7s"> 
						  <h6 class="orange mb-15px radius-50px pl-20px pt-7px pb-7px"><span class="mr-5px fs-30">-</span> What are the membership requirements? </h6>
                    	  <p class="pl-20px mb-15px"><ol><li>Must be above 18 years old.</li><li>Is of good character and sound mind.</li></ol></p>
						  <h6 class="blue mb-15px radius-50px pl-20px pt-7px pb-7px"><span class="mr-5px fs-30">-</span> How do I become a member? </h6>
                    	  <p class="pl-20px mb-15px">Interested people should fill a membership application form and pay an entrance fee of Ksh. 2,000 then submit fully completed form (scanned), scanned copy of National ID and KRA pin certificate for processing to membership@nsig.co.ke</p>
						  <h6 class="blue mb-15px radius-50px pl-20px pt-7px pb-7px"><span class="mr-5px fs-30">-</span> What is the minimum contribution? </h6>
                    	  <p class="pl-20px mb-15px">The minimum monthly contribution is Kshs. 1,000 payable by the tenth day of every month. A penalty of Kshs. 100 shall be charged for failure to pay in tume and Kshs. 10 for every additional day.</p>
						  <h6 class="blue mb-15px radius-50px pl-20px pt-7px pb-7px"><span class="mr-5px fs-30">-</span> What is the difference between member contribution and shares? </h6>
                    	  <p class="pl-20px mb-15px">
							  <ul>
								  <li>Contributions are refundable upon termination of membership(withdrawal) up tp 80% less any outstanding liabilities ti the Group.</li>
								  <li>Shares or share capital is the permanent member contribution towards the groups capital.</li>
								  <li>Shares cannot be withdrawn or transferred even on exit from the Group.</li>
								  <li>Each member must have 100 shares valued at Kshs. 5,000 in share capital unlike contributions which have no maximum amount.</li>
								  <li>The share capital must be paid up within six months of membership.</li>
							  </ul>
						  </p>
                      </div>
				  </div>
				  <div class="col-md-6">
					  <div class="box mt-25px mb-25px wow fadeInRight" data-wow-delay="0.45s"> 
						<h6 class="blue mb-15px radius-50px pl-20px pt-7px pb-7px"><span class="mr-5px fs-30">-</span> When does one qualify for a loan. </h6>
						<p class="pl-20px mb-15px">One qualifies to take a loan after six months of consistent contribution and paid up share capital to the Group.</p>
						<h6 class="blue mb-15px radius-50px pl-20px pt-7px pb-7px"><span class="mr-5px fs-30">-</span> What is the maximum loan one is entitled to? </h6>
						<p class="pl-20px mb-15px">One can borrow up tp three(3) times of the total contributions and share capital.</p>
						<h6 class="blue mb-15px radius-50px pl-20px pt-7px pb-7px"><span class="mr-5px fs-30">-</span> What is the interest rates on loans? </h6>
						<p class="pl-20px mb-15px">Loans will attract an interest rate of 1% per month on reducing balance.</p>
						<h6 class="blue mb-15px radius-50px pl-20px pt-7px pb-7px"><span class="mr-5px fs-30">-</span> Can I take more than one loan? </h6>
						<p class="pl-20px mb-15px">No. A member is required to take one loan at a time.</p>
					</div>
				  </div>
			  </div>
		  </div>
	  </section>
	  <!-- ==================================================
							End faqs
      ================================================== -->
	   
	  <!-- ==================================================
      ================================================== -->
	  <section class="bg-gray triangle-top triangle-bottom" data-scroll-index="5">
		<div class="container">
			<h1 class="title-h">Fill the Membership Form to Join</h1>
			<div class="row">
				<div class="col-md-6">
					<div class="mt-25px mb-25px">
						<div class="row">
							<div class="col-md-8">
								<div class="mt-10px mb-10px p-relative">
									<a href="../new-nig/docs/MEMBERSHIP APPLICATION FORM.pdf" target="_blank">
										<i class="im im-download p-absolute color-blue fs-35 bg-gray text-center radius-50 mb-25px transition-2"></i>
									</a>
									<div class="pl-60px">
										<h5>Membership Application Form</h5>
									</div>
								</div>
							</div>
							<div class="col-md-8">
								<div class="mt-10px mb-10px p-relative">
									<a href="../new-nig/docs/NEXT OF KIN.pdf" target="_blank">
										<i class="im im-download p-absolute color-blue fs-35 bg-gray text-center radius-50 mb-25px transition-2"></i>
									</a>
									<div class="pl-60px">
										<h5>Next of Kin</h5>
									</div>
								</div>
							</div>
							<div class="col-md-8">
								<div class="mt-10px mb-10px p-relative">
									<a href="../new-nig/docs/PROXY NOMINATION FORM.pdf" target="_blank">
										<i class="im im-download p-absolute color-blue fs-35 bg-gray text-center radius-50 mb-25px transition-2"></i>
									</a>
									<div class="pl-60px">
										<h5>Proxy Nomination</h5>
									</div>
								</div>
							</div>
							<div class="col-md-8">
								<div class="mt-10px mb-10px p-relative">
									<a href="../new-nig/docs/LOANS POLICY NIG.pdf" target="_blank">
										<i class="im im-download p-absolute color-blue fs-35 bg-gray text-center radius-50 mb-25px transition-2"></i>
									</a>
									<div class="pl-60px">
										<h5>Loans Policy</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="mt-25px mb-25px">
						<div class="row">
							<div class="col-md-8">
								<div class="mt-10px mb-10px p-relative">
									<a href="../new-nig/docs/GUARANTOR SUBSTITUTION FORM.pdf" target="_blank">
										<i class="im im-download p-absolute color-blue fs-35 bg-gray text-center radius-50 mb-25px transition-2"></i>
									</a>
									<div class="pl-60px">
										<h5>Guarantor Substitution Form</h5>
									</div>
								</div>
							</div>
							<div class="col-md-8">
								<div class="mt-10px mb-10px p-relative">
									<a href="../new-nig/docs/INVESTMENT POLICY NIG.pdf" target="_blank">
										<i class="im im-download p-absolute color-blue fs-35 bg-gray text-center radius-50 mb-25px transition-2"></i>
									</a>
									<div class="pl-60px">
										<h5>Investment Policy NIG</h5>
									</div>
								</div>
							</div>
							<div class="col-md-8">
								<div class="mt-10px mb-10px p-relative">
									<a href="../new-nig/docs/NSIG CONSTITUTION.pdf" target="_blank">
										<i class="im im-download p-absolute color-blue fs-35 bg-gray text-center radius-50 mb-25px transition-2"></i>
									</a>
									<div class="pl-60px">
										<h5>NIG Constitution</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- ==================================================
	================================================== -->
	
	  <!-- ==================================================
							footer-area
      ================================================== -->
	  <section class="footer-area sec-padding">
		  <div class="container">
			  <div class="row">
				  <div class="col-lg-4 col-sm-6">
					  <div class="mt-25px mb-25px">
						  <img src="{{ asset('landing/images/logo.png') }}" class="mb-20px" alt="img">
						  <p class="mb-20px">
							  Neptune Investments Group(NIG), established in September 2018, is a subsidiary of Neptune Investments Club(NIC), which was established in October 2013.
							  NIG is a Self help Group registered under the Ministry of Labour and Social Protection.
						  </p>
						  <a class="main-btn btn-3"  href="#" data-scroll-nav="1">Get Started</a>
					  </div>
				  </div>
				  <div class="col-lg-4 col-sm-6">
					  <div class="mt-25px mb-25px links">
						  <h4 class="mb-20px">Useful Links</h4>
						  <h6 class="mb-10px">
							  <i class="fa fa-angle-right"></i> <a  href="#" data-scroll-nav="1" class="color-333 color-blue-hvr transition-3">Home</a>
						  </h6>
						  <h6 class="mb-10px">
							  <i class="fa fa-angle-right"></i> <a  href="#" data-scroll-nav="2" class="color-333 color-blue-hvr transition-3">About</a>
						  </h6>
						  <h6 class="mb-10px">
							  <i class="fa fa-angle-right"></i> <a  href="#" data-scroll-nav="3" class="color-333 color-blue-hvr transition-3">Service</a>
						  </h6>
						  <h6 class="mb-10px">
							  <i class="fa fa-angle-right"></i> <a  href="#" data-scroll-nav="4" class="color-333 color-blue-hvr transition-3">FAQs</a>
						  </h6>
						  <h6 class="mb-10px">
							  <i class="fa fa-angle-right"></i> <a  href="#" data-scroll-nav="5" class="color-333 color-blue-hvr transition-3">Downloads</a>
						  </h6>
					  </div>
				  </div>
				  <div class="col-lg-4 col-sm-6">
					  <div class="mt-25px mb-25px">
						  <h4 class="mb-20px">Contact Info</h4>
						  <p class="mb-20px">For any Queries, reach us through:</p>
						  <h6><i class="fa fa-globe mr-5px fs-15 color-blue bg-gray radius-50 address text-center"></i> https://nsig.co.ke </h6>
						  <h6><i class="fa fa-phone mr-5px fs-15 color-blue bg-gray radius-50 address text-center"></i> +(254) 721 377 608 </h6>
						  <h6><i class="fa fa-phone mr-5px fs-15 color-blue bg-gray radius-50 address text-center"></i> +(254) 734 833 865 </h6>
						  <h6 class="mb-30px"><i class="fa fa-envelope mr-5px fs-15 color-blue bg-gray radius-50 address text-center"></i> info@nsig.co.ke </h6>
						  <a href="https://web.facebook.com/neptuneinvest" class="social color-blue color-fff-hvr bg-orange-hvr bg-gray text-center radius-50 fs-15 d-inline-block"><i class="fa fa-facebook"></i> </a> 
						  <a href="https://twitter.com/neptuneinvest?s=08" class="social color-blue color-fff-hvr bg-orange-hvr bg-gray text-center radius-50 fs-15 d-inline-block"><i class="fa fa-twitter"></i> </a> 
						  <a href="https://www.instagram.com/neptuneinvestmentsgroup/" class="social color-blue color-fff-hvr bg-orange-hvr bg-gray text-center radius-50 fs-15 d-inline-block"><i class="fa fa-instagram"></i> </a> 
					  </div>
				  </div>
			  </div>
		  </div>
	  </section>
	  <!-- ==================================================
							End footer-area
      ================================================== -->
	  
	  <!-- ==================================================
							copyright-area
      ================================================== -->
	  <section class="bg-gray sm-padding text-center">
		  <div class="container">
			  <h6 class="mb-0px">© 2021</h6>
		  </div>
	  </section>
	  <!-- ==================================================
							End copyright-area
      ================================================== -->
	  
	  
	  <!-- ==================================================
							scroll-top-btn
      ================================================== -->
	  <div class="scroll-top-btn text-center">
		  <i class="fa fa-angle-up fs-20 color-fff bg-blue bg-orange-hvr radius-50"></i>          
	  </div>
	  <!-- ==================================================
							End scroll-top-btn
      ================================================== -->
	  
	  

   <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('landing/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('landing/js/jquery-migrate-3.0.0.min.js') }}"></script>
    <script src="{{ asset('landing/js/popper.min.js') }}"></script>
    <script src="{{ asset('landing/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('landing/js/jquery.counterup.min.js') }}"></script>
	<script src="{{ asset('landing/js/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('landing/js/lity.min.js') }}"></script>
    <script src="{{ asset('landing/js/lightbox.js') }}"></script>
	<script src="{{ asset('landing/js/scrollIt.min.js') }}"></script>
	<script src="{{ asset('landing/js/validator.js') }}"></script>
	<script src="{{ asset('landing/js/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('landing/js/isotope.pkgd.min.js') }}"></script>
	<script src="{{ asset('landing/js/main.js') }}"></script>
	<script src="{{ asset('landing/js/wow.min.js') }}"></script>
	    
  </body>

</html>