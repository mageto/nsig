@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-6 col-lg-6 col-xl-3 mb-5">
        <div class="card card-tile card-xs bg-primary bg-gradient text-center">
            <div class="card-body p-4">
                <!-- Accepts .invisible: Makes the items. Use this only when you want to have an animation called on it later -->
                <div class="tile-left">
                    <i class="batch-icon batch-icon-user-alt batch-icon-xxl"></i>
                </div>
                <div class="tile-right">
                    <div class="tile-number">{{ number_format(App\Savings::where('memberId', Auth::user()->id)->sum('savings')) }}</div>
                    <div class="tile-description">Total Savings</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-6 col-xl-3 mb-5">
        <div class="card card-tile card-xs bg-secondary bg-gradient text-center">
            <div class="card-body p-4">
                <div class="tile-left">
                    <i class="batch-icon batch-icon-tag-alt-2 batch-icon-xxl"></i>
                </div>
                <div class="tile-right">
                    <div class="tile-number">${{ number_format(App\Loans::where('memberId', Auth::user()->id)->where('balance', '>=', 0)->count()) }}</div>
                    <div class="tile-description">Total Unpaid Loans</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-6 col-xl-3 mb-5">
        <div class="card card-tile card-xs bg-primary bg-gradient text-center">
            <div class="card-body p-4">
                <div class="tile-left">
                    <i class="batch-icon batch-icon-list batch-icon-xxl"></i>
                </div>
                <div class="tile-right">
                    <div class="tile-number">{{ number_format(App\Loans::where('memberId', Auth::user()->id)->where('balance', '<=', 0)->sum('amount')) }}</div>
                    <div class="tile-description">Total Paid Loans</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-6 col-xl-3 mb-5">
        <div class="card card-tile card-xs bg-secondary bg-gradient text-center">
            <div class="card-body p-4">
                <div class="tile-left">
                    <i class="batch-icon batch-icon-star batch-icon-xxl"></i>
                </div>
                <div class="tile-right">
                    <div class="tile-number">{{ number_format(App\Loans::where('memberId', Auth::user()->id)->where('balance', '>=', 0)->sum('balance')) }}</div>
                    <div class="tile-description">Total Loan Balance</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mb-5">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                Savings
            </div>
            <div class="card-table table-responsive">
              <div class="table-responsive">
                <table id="datatable-1" class="table table-datatable table-striped table-hover dataTable no-footer" role="grid" aria-describedby="datatable-1_info">
                  <thead class="thead-light">
                      <tr>
                        <th>#</th>
                          <th>Date</th>
                          <th>Amount</th>
                          <!-- <th>Receipt</th> -->
                      </tr>
                  </thead>
                  <tbody></tbody>
                </table>
                <input type="hidden" name="member_id" id="member_id" value="{{ Auth::user()->id }}">

                  <script type="text/javascript">
                    var member_id = document.getElementById('member_id').value;
                    var table;

                    $(document).ready(function() {
                    table = $('#datatable-1').DataTable({
                        processing: true,
                        serverSide: true,
                        responsive: true,
                        ajax: '{{ url("/show-member-savings/") }}' + '/' + member_id,
                        columns: [
                        { data: 'id',           name: 'id' ,         searchable: false},
                        { data: 'datePaid',     name: 'datePaid' ,   searchable: true},
                        { data: 'savings',      name: 'savings' ,    searchable: true},
                        // { data: "action" }
                        ],
                    });
                    });
                </script>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
