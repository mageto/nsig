
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="assets/img/logo.png">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'NIG') }}</title>

	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700&amp;subset=latin-ext" rel="stylesheet">

	<!-- CSS - REQUIRED - START -->
	<!-- Batch Icons -->
	<link rel="stylesheet" href="{{ asset('assets/fonts/batch-icons/css/batch-icons.css') }}">
	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap/bootstrap.min.css') }}">
	<!-- Material Design Bootstrap -->
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap/mdb.min.css') }}">
	<!-- Custom Scrollbar -->
	<link rel="stylesheet" href="{{ asset('assets/plugins/custom-scrollbar/jquery.mCustomScrollbar.min.css') }}">
	<!-- Hamburger Menu -->
	<link rel="stylesheet" href="{{ asset('assets/css/hamburgers/hamburgers.css') }}">

	<!-- CSS - REQUIRED - END -->

	<!-- CSS - OPTIONAL - START -->
	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{ asset('assets/fonts/font-awesome/css/font-awesome.min.css') }}">
	<!-- JVMaps -->
	<link rel="stylesheet" href="{{ asset('assets/plugins/jvmaps/jqvmap.min.css') }}">
	<!-- CSS - OPTIONAL - END -->

		<link href="{{ asset('js/toastr.min.css') }}" rel="stylesheet" />
	<!-- QuillPro Styles -->
	<link rel="stylesheet" href="{{ asset('assets/css/quillpro/quillpro.css') }}">
  <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
  <!-- Responsive datatable examples -->
  <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<!-- select 2 -->
  <link href="{{ asset('assets/plugins/multiselect/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />


  <script type="text/javascript" src="{{ asset('assets/js/jquery/jquery-3.1.1.min.js') }}"></script>
</head>

<body>

	<div class="container-fluid">
		<div class="row">
			<nav class="navbar-sidebar-horizontal navbar navbar-expand-lg navbar-light bg-white fixed-top">
				<a class="navbar-brand" href="#">
					<img src="{{ asset('assets/img/logo.png') }}" width="150" height="" alt="NIG">
				</a>
				<button class="hamburger hamburger--slider" type="button" data-target=".sidebar-horizontal" aria-controls="sidebar-horizontal" aria-expanded="false" aria-label="Toggle Main Menu">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</button>
				<!-- Added Mobile-Only Menu -->
				<ul style="float:right;" class="navbar-nav ml-auto mobile-only-control d-block d-sm-block d-md-block d-lg-none">
					<li><a class="btn btn-sm btn-danger" href="{{ route('logout') }}">Logout</a></li>
				</ul>

				<!--  DEPRECATED CODE:
					<div class="navbar-collapse" id="navbarSupportedContent">
				-->
				<!-- USE THIS CODE Instead of the Commented Code Above -->
				<!-- .collapse added to the element -->
				<div class="collapse navbar-collapse" id="navbar-header-content">
					<ul class="navbar-nav navbar-language-translation mr-auto">
						<li class="nav-item dropdown">
							<!-- <a class="nav-link dropdown-toggle" id="navbar-dropdown-menu-link" data-toggle="dropdown" data-flip="false" aria-haspopup="true" aria-expanded="false">
								<i class="batch-icon batch-icon-book-alt-"></i>
								English
							</a> -->
						</li>
					</ul>

          <ul class="navbar-nav ml-5 navbar-profile">
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" id="navbar-dropdown-navbar-profile" data-toggle="dropdown" data-flip="false" aria-haspopup="true" aria-expanded="false">
  								<div class="profile-name">
  								{{ Auth::user()->name }}
								</div>
								<div class="profile-picture bg-gradient bg-primary has-message float-right">
									<img src="avatar/{{ Auth::user()->avatar }}" width="44" height="44">
								</div>
							</a>
							<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-dropdown-navbar-profile">
								<li><a class="dropdown-item" href="member-profile">Profile</a></li
								<li><a class="dropdown-item" href="{{ route('logout') }}">Logout</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
      <div class="right-column">
        <?php if (Auth::user()->memberType == 'admin' || Auth::user()->memberType == 'treasurer') { ?>
          <nav class="sidebar-horizontal navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
  					<div class="navbar-collapse" id="navbar-header-menu-outer">
  						<ul class="navbar-nav navbar-header-menu mr-auto">
  							<li class="nav-item">
  								<a class="nav-link" href="/home">
  									<i class="batch-icon batch-icon-browser-alt"></i>
  									Dashboard
  								</a>
  							</li>
  							<li class="nav-item dropdown">
  								<a class="nav-link dropdown-toggle" id="navbar-dropdown-dashboard-link" data-toggle="dropdown" data-flip="false" aria-haspopup="true" aria-expanded="false">
  									<i class="batch-icon batch-icon-layout-content-left"></i>
  									Savings <span class="sr-only">(current)</span>
  								</a>
  								<ul class="dropdown-menu" aria-labelledby="navbar-dropdown-dashboard-link">
  									<li><a class="dropdown-item" href="/savings"> Approved Savings </a></li>
  									<?php if (Auth::user()->memberType == 'treasurer') { ?>
									  <li><a class="dropdown-item" href="/pending-savings"> Pending Savings </a></li>
									  <?php } ?>
  								</ul>
  							</li>

							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" id="navbar-dropdown-dashboard-link" data-toggle="dropdown" data-flip="false" aria-haspopup="true" aria-expanded="false">
									<i class="batch-icon batch-icon-layout-content-left"></i>
									Members <span class="sr-only">(current)</span>
								</a>
								<ul class="dropdown-menu" aria-labelledby="navbar-dropdown-dashboard-link">
									<li><a class="dropdown-item" href="/all-members"> All Members </a></li>
									<li><a class="dropdown-item" href="/active-members"> Active Members </a></li>
									<li><a class="dropdown-item" href="/inactive-members"> Inactive Members</a></li>
								</ul>
							</li>
  							<li class="nav-item dropdown">
  								<a class="nav-link dropdown-toggle" id="navbar-dropdown-ecommerce-link" data-toggle="dropdown" data-flip="false" aria-haspopup="true" aria-expanded="false">
  									<i class="batch-icon batch-icon-store"></i>
  									Loans
  								</a>
  								<ul class="dropdown-menu" aria-labelledby="navbar-dropdown-ecommerce-link">
  									<li><a class="dropdown-item" href="/all-loans">All Loans</a></li>
  									<li><a class="dropdown-item" href="/paid-loans">Paid Loans</a></li>
  									<li><a class="dropdown-item" href="/unpaid-loans">Unpaid Loans</a></li>
  									<li><a class="dropdown-item" href="/pending-loan-requests">Pending Loan Requests</a></li>
  									<li><a class="dropdown-item" href="/loan-types">Loan Types</a></li>
  								</ul>
  							</li>
  							<li class="nav-item">
  								<a class="nav-link" href="/messages">
  									<i class="batch-icon batch-icon-mail"></i>
  									Messages
  								</a>
  							</li>
  						</ul>
  					</div>
  				</nav>
        <?php } elseif (Auth::user()->memberType == 'member') { ?>
            <nav class="sidebar-horizontal navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    					<div class="navbar-collapse" id="navbar-header-menu-outer">
    						<ul class="navbar-nav navbar-header-menu mr-auto">
    							<li class="nav-item">
    								<a class="nav-link" href="/member">
    									<i class="batch-icon batch-icon-browser-alt"></i>
    									Dashboard
    								</a>
    							</li>
	    							<li class="nav-item">
	    								<a class="nav-link" href="/member-savings">
	    									<i class="batch-icon batch-icon-plus"></i>
	    									Savings
	    								</a>
	    							</li>
    							<li class="nav-item dropdown">
    								<a class="nav-link dropdown-toggle" id="navbar-dropdown-dashboard-link" data-toggle="dropdown" data-flip="false" aria-haspopup="true" aria-expanded="false">
    									<i class="batch-icon batch-icon-layout-content-left"></i>
    									Loans({{ number_format(App\Guarantorship::where('status','Pending')->where('guarantorId', Auth::user()->id)->count()) }}) <span class="sr-only">(current)</span>
    								</a>
    								<ul class="dropdown-menu" aria-labelledby="navbar-dropdown-dashboard-link">
  										<li><a class="dropdown-item" href="/apply-loan">Apply Loan</a></li>
    									<li><a class="dropdown-item" href="/all-member-loans"> All loans </a></li>
    									<li><a class="dropdown-item" href="/member-loan-repayments"> Loan Repayments </a></li>
    									<li><a class="dropdown-item" href="/member-loan-request"> Loan Request </a></li>
    									<li><a class="dropdown-item" href="/guarantor-requests"> Guarantor Requests({{ number_format(App\Guarantorship::where('status','Pending')->where('guarantorId', Auth::user()->id)->count()) }}) </a></li>
    								</ul>
    							</li>
    						</ul>
    					</div>
    				</nav>
        <?php  } ?>

        <main class="main-content p-5" role="main">
        			@yield('content')
					<div class="row mb-5">
						<div class="col-md-12">
							<footer>
								Powered by - <a href="http://nsig.co.ke/" target="_blank" style="font-weight:300;color:#ffffff;background:#1d1d1d;padding:0 3px;">Neptune</a>
							</footer>
						</div>
					</div>
				</main>
			</div>
		</div>
	</div>

	<!-- SCRIPTS - REQUIRED START -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- Bootstrap core JavaScript -->
	<!-- JQuery -->
	<script type="text/javascript" src="{{ asset('assets/js/jquery/jquery-3.1.1.min.js') }}"></script>
	<!-- Popper.js - Bootstrap tooltips -->
	<script type="text/javascript" src="{{ asset('assets/js/bootstrap/popper.min.js') }}"></script>
	<!-- Bootstrap core JavaScript -->
	<script type="text/javascript" src="{{ asset('assets/js/bootstrap/bootstrap.min.js') }}"></script>
	<!-- MDB core JavaScript -->
	<script type="text/javascript" src="{{ asset('assets/js/bootstrap/mdb.min.js') }}"></script>
	<!-- Velocity -->
	<script type="text/javascript" src="{{ asset('assets/plugins/velocity/velocity.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/plugins/velocity/velocity.ui.min.js') }}"></script>
	<!-- Custom Scrollbar -->
	<script type="text/javascript" src="{{ asset('assets/plugins/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
	<!-- jQuery Visible -->
	<script type="text/javascript" src="{{ asset('assets/plugins/jquery_visible/jquery.visible.min.js') }}"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script type="text/javascript" src="{{ asset('assets/js/misc/ie10-viewport-bug-workaround.js') }}"></script>

	<!-- SCRIPTS - REQUIRED END -->

	<!-- SCRIPTS - OPTIONAL START -->
	<!-- ChartJS -->
	<script type="text/javascript" src="{{ asset('assets/plugins/chartjs/chart.bundle.min.js') }}"></script>
	<!-- JVMaps -->
	<script type="text/javascript" src="{{ asset('assets/plugins/jvmaps/jquery.vmap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/plugins/jvmaps/maps/jquery.vmap.usa.js') }}"></script>
	<!-- Image Placeholder -->
	<script type="text/javascript" src="{{ asset('assets/js/misc/holder.min.js') }}"></script>
	<!-- SCRIPTS - OPTIONAL END -->

		  <script src="{{ asset('js/toastr.min.js') }}"></script>
	<!-- QuillPro Scripts -->
	<script type="text/javascript" src="assets/js/scripts.js"></script>  <!-- Required datatable js -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <!-- Buttons examples -->
    <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.colVis.min.js') }}"></script>
	<!-- Form Wizard -->
	<script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}"></script>
    <!-- Responsive examples -->
    <script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
	<!-- select 2 -->
    <script src="{{ asset('assets/plugins/multiselect/js/jquery.multi-select.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/jquery-quicksearch/jquery.quicksearch.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/js/select2.full.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/jquery.formadvanced.init.js') }}"></script>

      <script type="text/javascript">
      $(document).ready(function() {
          $('#datatable').DataTable();

          //Buttons examples
          var table = $('#datatable-buttons').DataTable({
              lengthChange: false,
              buttons: ['copy', 'excel', 'pdf', 'colvis']
          });

          table.buttons().container()
                  .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
      } );

  </script>
</body>
</html>
