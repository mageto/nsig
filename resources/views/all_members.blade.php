@extends('layouts.app')

@section('content')

<div class="row mb-5">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                User Management
            <button style="float:right !important;" type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addNewMemberModal">
                Add New Member
            </button>
            </div>
            <br>
            <div class="table-responsive">
              <table id="datatable-1" class="table table-datatable table-striped table-hover dataTable no-footer" role="grid" aria-describedby="datatable-1_info">
                  <thead class="thead-light">
                      <tr>
                        <th>#</th>
                          <th>Member</th>
                          <th>Email</th>
                          <th>Phone</th>
                          <th>ID Number</th>
                          <th class="text-right">Action</th>
                      </tr>
                  </thead>
                  <tbody></tbody>
								</table>

                <script type="text/javascript">
                  var table;

                  $(document).ready(function() {
                  table = $('#datatable-1').DataTable({
                      processing: true,
                      serverSide: true,
                      responsive: true,
                      ajax: '{{ url("/show-members") }}',
                      columns: [
                      { data: 'id',        name: 'id' ,         searchable: false},
                      { data: 'name',      name: 'name' ,       searchable: true},
                      { data: 'email',     name: 'email' ,      searchable: true},
                      { data: 'phone',     name: 'phone' ,      searchable: true},
                      { data: 'idNumber',  name: 'idNumber' ,   searchable: true},
                      { data: "action" }
                      ],
                  });
                  });
              </script>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addNewMemberModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Members</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                  <form>
                      <div class="form-group">
                          <label for="name">Full Name</label>
													<input class="form-control form-control-sm" name="name" id="name" type="text" placeholder="Full Names">
											</div>
                      <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control form-control-sm" name="email" id="email" placeholder="name@example.com">
                      </div>
											<div class="form-group">
                        <label for="gender">Gender</label>
												<select class="form-control form-control-sm" id="gender" name="gender">
                          <option>Gender</option>
                          <option value="Male">Male</option>
													<option value="Female">Female</option>
												</select>
											</div>
                      <div class="form-group">
                          <label for="idNumber">ID Number</label>
													<input class="form-control form-control-sm" id="idNumber" name="idNumber" type="text" placeholder="ID Number">
											</div>
                      <div class="form-group">
                          <label for="phone">Phone Number</label>
													<input class="form-control form-control-sm" id="phone" name="phone" type="text" placeholder="Phone Number">
											</div>
                      <div class="form-group">
                          <label for="dob">D.O.B</label>
													<input class="form-control form-control-sm" id="dob" name="dob" type="date">
											</div>
                      <div class="form-group">
                        <label for="amount">Amount</label>
                        <input class="form-control form-control-sm" id="amount" name="amount" type="text" placeholder="10000">
                        <input class="form-control form-control-sm" id="member_id" name="member_id" type="text">
					</div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                    <button type="button" onclick="add_member()" id="btn-save" class="btn btn-primary btn-sm">Submit</button>
                    <button type="button" onclick="update_member()" id="btn-save" class="btn btn-primary btn-sm">Update</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function edit_member(member_id){
    document.getElementById('member_id').value = member_id;
        $.get('/get-member/' + member_id, function (data) {

            $('#btn-save').val("Update");
            console.log(data);
            $('#member_id').val(data[0].id);
            $('#name').val(data[0].name);
            $('#email').val(data[0].email);
            $('#gender').val(data[0].gender).change();
            $('#idNumber').val(data[0].idNumber);
            $('#phone').val(data[0].phone);
            $('#dob').val(data[0].dob).change();
          });
        }
    function add_member(){
      var name        =  document.getElementById('name').value;
      var email       =  document.getElementById('email').value;
      var gender      =  document.getElementById('gender').value;
      var idNumber    =  document.getElementById('idNumber').value;
      var dob         =  document.getElementById('dob').value;
      var phone       =  document.getElementById('phone').value;
      var amount      =  document.getElementById('amount').value;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
                }
            });

            $.ajax({
                url: '/add-members', // point to server-side PHP script
                data: { name : name, email : email, gender : gender, idNumber : idNumber, dob : dob, phone : phone, amount : amount  } ,
                type: 'POST',
                success: function(data) {
                    if (data == 1) {
                        toastr.error('ID Number Already Exists');
                    }else if (data == 2) {
                        toastr.error('Email Already Exists');
                    } else {
                        document.getElementById('name').val = "";
                        document.getElementById('email').val = "";
                        document.getElementById('gender').val = "";
                        document.getElementById('idNumber').val = "";
                        document.getElementById('dob').val = "";
                        document.getElementById('phone').val = "";
                        table.ajax.reload(null,false); //reload datatable ajax
                        $('#addNewMemberModal').modal('hide');
                        toastr.success('Member was added successfully');
                    }

                }
            });
    }
    function update_member(){
      var name        =  document.getElementById('name').value;
      var email       =  document.getElementById('email').value;
      var gender      =  document.getElementById('gender').value;
      var idNumber    =  document.getElementById('idNumber').value;
      var dob         =  document.getElementById('dob').value;
      var phone       =  document.getElementById('phone').value;
      var amount      =  document.getElementById('amount').value;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
                }
            });

            $.ajax({
                url: '/update-members', // point to server-side PHP script
                data: { name : name, email : email, gender : gender, idNumber : idNumber, dob : dob, phone : phone, amount : amount  } ,
                type: 'POST',
                success: function(data) {
                    document.getElementById('name').val = "";
                    document.getElementById('email').val = "";
                    document.getElementById('gender').val = "";
                    document.getElementById('idNumber').val = "";
                    document.getElementById('dob').val = "";
                    document.getElementById('phone').val = "";
                    table.ajax.reload(null,false); //reload datatable ajax
                    $('#addNewMemberModal').modal('hide');
                    toastr.success('Member was updated successfully');
                }
            });
    }
</script>
@endsection
