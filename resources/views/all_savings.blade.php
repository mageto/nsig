@extends('layouts.app')

@section('content')

<div class="row mb-5">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                Savings Management
            <button style="float:right !important;" type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addNewSavingsModal">
                Add New Savings
            </button>
            </div>
            <br>
            <div class="table-responsive">
              <table id="datatable-1" class="table table-datatable table-striped table-hover dataTable no-footer" role="grid" aria-describedby="datatable-1_info">
                  <thead class="thead-light">
                      <tr>
                        <th>#</th>
                          <th>Member</th>
                          <th>ID Number</th>
                          <th>Amount</th>
                          <th>Date</th>
                          <th class="text-right">Action</th>
                      </tr>
                  </thead>
                  <tbody></tbody>
								</table>

                <script type="text/javascript">
                  var table;

                  $(document).ready(function() {
                  table = $('#datatable-1').DataTable({
                      processing: true,
                      serverSide: true,
                      responsive: true,
                      ajax: '{{ url("/show-savings") }}',
                      columns: [
                      { data: 'id',        name: 'id' ,         searchable: false},
                      { data: 'name',      name: 'name' ,       searchable: true},
                      { data: 'idNumber',  name: 'idNumber' ,   searchable: true},
                      { data: 'savings',   name: 'savings' ,    searchable: true},
                      { data: 'datePaid',  name: 'datePaid' ,   searchable: true},
                      { data: "action" }
                      ],
                  });
                  });
              </script>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addNewSavingsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Members</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                  <form>
                      <div class="form-group">
                        <select class="form-control form-control-sm" id="memberId" name="memberId">
                          <option>Choose Member</option>
                          <?php foreach ($members as $value) { ?>
                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="form-group">
                          <label for="amount">Amount</label>
													<input class="form-control form-control-sm" id="amount" name="amount" type="text" placeholder="Amount">
											</div>
                      <div class="form-group">
                          <label for="transactionRef">Transaction Ref:</label>
													<input class="form-control form-control-sm" id="transactionRef" name="transactionRef" type="text" placeholder="Transaction Ref">
											</div>
                      <div class="form-group">
                          <label for="datePaid">Date</label>
													<input class="form-control form-control-sm" id="datePaid" name="datePaid" type="date">
											</div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                    <button type="button" onclick="add_saving()" id="btn-save" class="btn btn-primary btn-sm">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function edit_member(member_id){

        $.get('/get-savings/' + member_id, function (data) {

            $('#btn-save').val("Update");
            console.log(data);

            $('#memberId').val(data[0].id).change();
            $('#amount').val(data[0].amount);
            $('#transactionRef').val(data[0].transactionRef);
            $('#datePaid').val(data[0].datePaid);
          });
        }
    function add_saving(){
      var memberId        =  document.getElementById('memberId').value;
      var amount          =  document.getElementById('amount').value;
      var transactionRef  =  document.getElementById('transactionRef').value;
      var datePaid        =  document.getElementById('datePaid').value;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
                }
            });

            $.ajax({
                url: '/add-savings', // point to server-side PHP script
                data: { memberId : memberId, amount : amount, transactionRef : transactionRef, datePaid : datePaid  } ,
                type: 'POST',
                success: function(data) {

                        document.getElementById('memberId').val = "";
                        document.getElementById('amount').val = "";
                        document.getElementById('transactionRef').val = "";
                        document.getElementById('datePaid').val = "";

                        table.ajax.reload(null,false); //reload datatable ajax
                        $('#addNewSavingsModal').modal('hide');
                        toastr.success('Savings added successfully');

                },
                error: function (error, data) {
                    console.log('Error:', data);
                }
            });
    }
</script>
@endsection
