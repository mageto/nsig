@extends('layouts.app')

@section('content')
<div class="row">
    <h4 class="offset-5 header-title mt-10 mb-10">Send Message</h4>
    <div class="card col-md-5 offset-1">
            <h4 style="margin-top:20px;" class="header-title mt-20 mb-30">Send Message to All Members</h4>
            <form class="offset-1 form-horizontal" role="form">
                <input type="hidden" class="form-control" name="all_client" id="all_client" placeholder="Phone">
                <br>
                <div class="form-group">
                    <label class="col-md-3 control-label">Message</label>
                    <div class="col-md-9">
                        <textarea name="message" class="form-control" rows="5"></textarea>
                    </div>
                </div>
                <br>
                <div class="form-group mb-0">
                    <div class="col-sm-9">
                    <button type="button" onclick="send_to_all()" class="btn btn-block btn-info waves-effect waves-light">Send</button>
                    </div>
                </div>
                <br>
            </form>
    
    </div>
    <div class="col-md-1"></div>
    <div class="card col-md-4">
            <h4 style="margin-top:10px;"  class=" header-title mt-10 mb-30">Send Message One Member</h4>
            <form class="offset-1 form-horizontal" role="form">
                {!! csrf_field() !!}
                <div class="form-group">
                    <label for="phone" class="col-sm-5 control-label">Phone Number</label>
                    <div class="col-sm-9">
                    <select class="select2 form-control" name="phone" id="phone" data-placeholder="Choose ...">
                            <option value="">Choose members</option>
                            <?php foreach ($members as $value) { ?>
                                <option value="{{ $value->phone }}">{{ $value->name }}, {{ $value->phone }}</option>
                            <?php } ?>
                        </select>
                    <!-- <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone"> -->
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Message</label>
                    <div class="col-md-9">
                        <textarea id="single_message" class="form-control" rows="5"></textarea>
                    </div>
                </div>
                <div class="form-group mb-30">
                    <div class="col-sm-9">
                    <button type="button" onclick="send_single_sms()" class="btn btn-block btn-info waves-effect waves-light">Send</button>
                    </div>
                </div>
            </form>
    
    </div>
</div>
<!-- Page-Title -->
<div class="row">
        <h4 class="page-title">Message</h4>
        
        <div class="card col-sm-5">
        </div><!-- end col -->
        <div class="col-sm-2"></div>
        <div class="card col-sm-5">
        </div><!-- end col -->
</div>
<!-- end row -->
<script>
function send_single_sms(){
    var single_message  =  document.getElementById('single_message').value;
    var phone  =  document.getElementById('phone').value;
    
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            }
        });

        $.ajax({
            url: '/send-message', // point to server-side PHP script
            data: { single_message : single_message, phone : phone } ,
            type: 'POST',
            success: function(data) {

                    document.getElementById('single_message').val = "";
                    document.getElementById('phone').val = "";

                    toastr.success('Message sent successfully.');

            },
            error: function (error, data) {
                console.log('Error:', data);
            }
        });
}
function send_to_all(){
    var message  =  document.getElementsByName('message')[0].value;
    alert(message);
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            }
        });

        $.ajax({
            url: '/send-message-all', // point to server-side PHP script
            data: { message : message } ,
            type: 'POST',
            success: function(data) {

                    document.getElementsByName('message').val = null;

                    toastr.success('Message sent successfully.');

            },
            error: function (error, data) {
                console.log('Error:', data);
            }
        });
}

</script>
@endsection