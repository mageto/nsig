<?php
include('header.php');
?>

		<div class="content top-30">
      <h3 class="uppercase bolder center-text">My <span class="color-highlight"> Savings</span></h3>
			<table class="table-borders-dark">
				<tr>
					<th>#</th>
					<th>Date</th>
					<th>Amount</th>
				</tr>
				<tr>
					<td>1</td>
					<td>06-06-2018</td>
					<td>1000</td>
				</tr>
				<tr>
					<td>2</td>
					<td>06-06-2018</td>
					<td>1000</td>
				</tr>
				<tr>
					<td>3</td>
					<td>06-06-2018</td>
					<td>1000</td>
				</tr>
				<tr>
					<td>4</td>
					<td>06-06-2018</td>
					<td>1000</td>
				</tr>
				<tr>
					<td>5</td>
					<td>06-06-2018</td>
					<td>1000</td>
				</tr>
				<tr>
					<td>6</td>
					<td>06-06-2018</td>
					<td>1000</td>
				</tr>
			</table>
</div>
<?php
include('footer.php');
?>
